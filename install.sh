#!/bin/sh

#  install.sh
#
#
#  Created by Eric Rothstein on 10/10/19.
#

status=0

run () {
  test $status = 0 || exit $status

  $*
  status=$?
}

echo "---------------------------------------------------------"
echo "Installing Classification Artefact"
echo "---------------------------------------------------------"

cd parser
run ghc -o parseaig Main.hs
cd ..

echo "---------------------------------------------------------"
echo "Installing Analytics Artefact"
echo "---------------------------------------------------------"

cd analysis
ghc -o analyse Main.hs
cd ..

echo "---------------------------------------------------------"
echo "Making Shell Scripts Executable"
echo "---------------------------------------------------------"

chmod +x classify.sh
chmod +x replicate6s106.sh
chmod +x replicate6s255.sh
chmod +x replicatenusmvdme2d3multi.sh
chmod +x replicatepdtvsarmultip.sh
chmod +x replicateTables123.sh

echo "Done!"
