#!/bin/sh

#  
#
#  Created by Eric Rothstein
#
cd replication
cp nusmvdme2d3multi.aig nusmvdme2d3multi-I.aig
cp nusmvdme2d3multi.aig nusmvdme2d3multi-M.aig
cp nusmvdme2d3multi.aig nusmvdme2d3multi-IM.aig
cd ..

./classify.sh replication/nusmvdme2d3multi.aig
./classify.sh -i replication/nusmvdme2d3multi-I.aig
./classify.sh -m replication/nusmvdme2d3multi-M.aig
./classify.sh -i -m replication/nusmvdme2d3multi-IM.aig

echo "This script just generated 4 txt files with prefix nusmvdme2d3multi and suffix Result.txt. -I means isolation was disabled, -M means monotonicity was disabled and -IM means that both were disabled."
echo ""
echo "The last line of those files shows both the average coverage and the execution time (among other data). The other lines describe information for each individual requirement."

echo "Bear in mind that the average results may vary if the VirtualMachine runs out of memory during SAT solving. In such a case, we recommend that you increase the size of the swap file and/or increase available memory."
