#!/bin/sh

#  
#
#  Created by Eric Rothstein on 10/10/19.
#
cd replication
cp pdtvsarmultip.aig pdtvsarmultip-I.aig
cp pdtvsarmultip.aig pdtvsarmultip-M.aig
cp pdtvsarmultip.aig pdtvsarmultip-IM.aig
cd ..

./classify.sh replication/pdtvsarmultip.aig
./classify.sh -i replication/pdtvsarmultip-I.aig
./classify.sh -m replication/pdtvsarmultip-M.aig
./classify.sh -i -m replication/pdtvsarmultip-IM.aig

echo "This script just generated 4 txt files with prefix pdtvsarmultip and suffix Result.txt. -I means isolation was disabled, -M means monotonicity was disabled and -IM means that both were disabled."
echo ""
echo "The last line of those files shows both the average coverage and the execution time (among other data). The other lines describe information for each individual requirement."

echo "Bear in mind that the average results may vary if the VirtualMachine runs out of memory during SAT solving. In such a case, we recommend that you increase the size of the swap file and/or increase available memory."
