module Quantification
( process,
)
where
import qualified Data.ByteString as Str
import Data.Binary.Get
import Data.Binary.Put
import Data.Array
import qualified Data.List.Split as Split
import Control.Monad
import qualified System.Directory as Directory
import System.IO
import System.Directory
import System.Environment
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import qualified Debug.Trace as Debug
import Data.List
import qualified Data.Text.IO as TextIO
import qualified Data.Text as Text

attackerLimit::Integer
attackerLimit = 3

--hardLimit = 20 -- set hardlimit to a value such that results beyond 2^hardLimit are of no statistical relevance... or your machine cannot compute them in a reasonable amount of time.

--type Result = Result{requirement::Integer, vulnerable::Bool, time::(Either Integer String), components::Integer, numMinimals::Integer, numSatCalls::Integer, averageMinimal::Maybe Double, coverage::Double} deriving (Eq)
type Result = (Integer,Bool,Either Integer String, Integer,Integer, Integer,Maybe Double,Double)
--the data type is not really necessary since we do not do any pattern matching.

data GlobalResult = Global{outOfMemory::Double,averageTime::Double,averageComp::Double,averageNumberOfMinimal::Double,averageSatCalls::Double, averageComponentsPerMinimal::Double,  averageCoverage::Double} deriving (Eq)

-- instance Show Result where
-- show (Result e b t c sa sat av cov) =
showResult::(Integer,Bool,Either Integer String, Integer,Integer, Integer,Maybe Double,Double)->String
showResult (e, b, t, c, sa, sat, av, cov) =
  let
    av' = case av of
      Nothing -> "--"
      Just v -> show v
  in
    (show e)++"\t"++
    (case t of Left t' -> show t'; Right t' -> (t'))++"\t"++
    (show c)++"\t"++
    (show sa)++"\t"++
    (show sat)++"\t"++
    (av')++"\t"++
    --(show ((fromIntegral fa)+sa))++"\t"++
    (show cov)

instance Show GlobalResult where
  show (Global oom t c ma sat co cov) =
    (show oom)++"\t"++
    (show t)++"\t"++
    (show c)++"\t"++
    (show ma)++"\t"++
    (show sat)++"\t"++
    (show co)++"\t"++
    (show cov)



--Number of successful attackers used to extrapolate coverage using monotonicity.
--Since we use inclusion-exclusion, we need to combine attackers, making the computation complexity 2^sampleSize

process monotonicity timeout sampleSize includeHeaders computeAverages benchmark  =
  do
    --We need to go to the benchmark directory
    currentDirectory <- getCurrentDirectory
    setCurrentDirectory "../parser/"
    directoryContents <- getDirectoryContents benchmark--(currentDirectory++"/cadical/"++benchmark)
    currentDirectory <- getCurrentDirectory
    --I think that quantification should be also percentual per requirement, but also per attacker?
    --Let us require having the aag in the directory as well.
    let
      directoryPrefix = currentDirectory++"/"++benchmark++"/"
      textFiles = filter (\filename -> case stripPrefix ("txt.") (reverse filename) of Nothing -> False; otherwise -> True) directoryContents
      clusterFiles = map (directoryPrefix++) $filter (\filename -> case stripPrefix (benchmark++"-cluster") filename of Nothing -> False; otherwise -> True) textFiles
    --   infoFile =
    --     case find (\filename -> filename =="info.txt") textFiles of
    --       Just i -> i
    --       otherwise -> error ("I did not find an info.txt file in "++directoryPrefix)
    --   benchmarkAag = directoryPrefix++benchmark++".aag"
    -- benchmarkInfo <- readFile benchmarkAag
    if includeHeaders then
      putStrLn ("Req.\t"++"Time\t"++"Comp.\t"++"#Suc.\t"++"#SAT\t"++"Av. Comp. per Suc.\t"++"Cov.")
    else
      return ()
    -- let
    --   (numVariables,numInputs,numLatches,numOutputs,numGates,numBadVariables,numConstraints) = parseAIGERHeader (head (lines benchmarkInfo))
    --   numAttackers = (2 ^ numLatches)
      --numReqs = toInteger numBadVariables
      --numQuestions = numAttackers*numReqs
    --Now, each requirement has, potentially
    --Maybe this can be transformed into a function that does the fold to return global, but has an IO side effect to print the reqResults.
    --fileContents <- mapM readFile clusterFiles
      --now, from each of the cluster files, I need to obtain a tally of how many attackers we found.
      --sortedFiles = fileContents--sortBy (\s t -> (length $ lines s) `compare` (length $ lines t)) fileContents

      --Here is where we have a problem. I think I should not use  use map
      --(reqResults,averages) = unzip $ map (processFile monotonicity timeout sampleSize ) sortedFiles
      --Maybe this can be transformed into a function that does the fold to return global, but has an IO side effect to print the reqResults.
    (global@(Global oom'' t'' c'' ma'' sat'' av'' cov''),badReq,checkedReq, totalReq) <-
      foldM
        (\((Global oom t c ma sat av cov),badReq,checkedReq,totalReq) clusterFile ->
          do
            --handle <- openFile clusterFile ReadMode
            --fileContents<- hGetContents handle
            fileContents <- TextIO.readFile clusterFile
            res@((Global oom' t' c' ma' sat' av' cov'),badReq',checkedReq', totalReq') <- processFile monotonicity timeout sampleSize fileContents
            let
                oom''= oom+oom'
                t''= t + t'
                c''=c+c'
                ma''=ma+ma'
                sat''=sat+sat'
                av''=av+av'
                cov''=cov+cov'
                badReq''=badReq+badReq'
                checkedReq''=checkedReq'+checkedReq
                totalReq''=totalReq'+totalReq
              in
                do
                  --mapM_ (putStrLn) (map showResult reqResults')
                  return $ res `seq`((Global oom'' t'' c'' ma'' sat'' av'' cov''),badReq'',checkedReq'', totalReq'')
                )
                  ((Global 0 0 0 0 0 0 0),0,0,0) clusterFiles--averages
    let
      finalResults = (Global (oom''/checkedReq) (t''/checkedReq) (c''/checkedReq) (ma''/checkedReq) (sat''/checkedReq) (av''/badReq) (cov''/totalReq))
    let
      theHeaders =
        if includeHeaders then
          "P(OoM)\t"++"Av.Time\t"++"Av.Comp.\t"++"Av.#Suc.\t"++"Av.#SAT\t"++"Av.Comp. per Suc.\t"++"Av.Cov."
        else
          ""
      finalReport = theHeaders++"\n"++(show finalResults)
    if computeAverages then
      putStrLn (finalReport)
    else
      return ()


--checkFile::String->i
--now, there are several things we could do with these requirement files. Ultimately we want:
--info on which attackers
processFile::Bool->Int->Int->Text.Text->IO (GlobalResult,Double,Double,Double)
processFile monotonicity timeout sampleSize  fileContents =
  do
    let
      --split the file into requirements
      (info:requirementReports)=Text.splitOn (Text.pack "#") fileContents
      --process requirements
      --Here we can do a fold as well instead of a map.
      checkedRequirements = length requirementReports
      allRequirements =
        if Text.length info > 0 then
          read (Text.unpack $ Text.tail info)::Int
        else
          checkedRequirements
    (global,badReq) <- foldM
      (\res@((Global oom' t' c' ma' sat' av' cov'),badReq') report  ->
        do
          let
            result@(e,b,t,c,ma,sat,av,cov) = processRequirement monotonicity timeout sampleSize report
            oom''=
              case t of
                Right _ -> oom'+1
                otherwise -> oom'
            t''=
              case t of
                Left t -> (fromIntegral t)+t'
                otherwise -> t'
            c''=(fromIntegral c)+c'
            ma''=(fromIntegral ma)+ma'
            sat''=(fromIntegral sat)+sat'
            av''=case av of
              Nothing -> av'
              Just x -> x+ av'
            cov''=cov+cov'
            badReq''=
              if ma > 0 then
                badReq'+1
              else
                badReq'
          putStrLn (showResult result)
          return $res `seq` ((Global oom'' t'' c'' ma'' sat'' av'' cov''),badReq'')
          )
        ((Global 0 0 0 0 0 0 0),0) requirementReports
    return (global,fromIntegral badReq, fromIntegral checkedRequirements, fromIntegral allRequirements)

-- what info is useful per requirement?
-- its expression
-- number of components
-- how many attackers were checked.
-- if r^max was able to break it.
-- attackers that successfully broke it
-- the time it took to break it
processRequirement::Bool->Int->Int->Text.Text->Result
processRequirement monotonicity timeout sampleSize  requirementReport =
  let
    --first we must separate the requirementReport on lines
    contents = Text.lines requirementReport
    --the first number is always the requirement expression
    requirementExpression =
      if length contents > 0 then
        read (Text.unpack $ head contents) :: Int
      else
        error ("maybe the file "++(Text.unpack requirementReport)++" is malformed?")
    time =
        if length (Text.unpack $ last contents) > 0 then
          if (head $ Text.unpack $ last contents) == '@' then
            let
              duration = read (tail $ Text.unpack $ last contents) :: Int -- this line starts with an @
            in
              if duration > timeout then
                Left $fromIntegral duration
              else
                Left $fromIntegral duration
          else
            Right "OoM"
        else
          error ("maybe the file "++(Text.unpack requirementReport)++" is malformed?")
    -- next, we process the attacker reports
    attackerReports =
      if length contents > 1 then
        init (tail contents)
      else
        error ("maybe the file "++(Text.unpack requirementReport)++" is malformed?")
    allAttackers = map processAttacker attackerReports
    --attackers = take (2^(hardLimit)) $ allAttackers
    attackers = allAttackers
    (r_MaxSuccessful, components) =
      if length attackers > 0 then
        head attackers --We can obtain the components relevant to this requirement from r^max, since it uses them all.
      else
        error ("The chosen hard limit is not supported. Try reducing it.")
    (minimalAttackers,countComponentsInMinimals,numFailedAttackers)=
      let
        initValue = if r_MaxSuccessful then ([],0,0) else ([],0,1)
        candidates = if length attackers > 0 then (tail attackers) else error ("there are no attacker reports for "++(Text.unpack requirementReport))
      in
        foldl' (\ (succeed,comp,failed) (s,cs)-> if s then (cs:succeed,(length cs)+comp,failed) else (succeed,comp,failed+1)) initValue candidates

    (combinable,sumable) =
      splitAt sampleSize $
      concat $ map sort $
      groupBy (\x y -> (length x) == (length y)) $
      sortBy (\x y -> (length x) `compare` (length y)) minimalAttackers --span (\x -> length x < 2) minimalAttackers --Attackers already sorted by number of components.
    justAddThese = fromIntegral$ (length sumable)+((length allAttackers)-(length attackers)) --beyond the sample size, we consider them to be failed attackers ONLY for the purposes of approximating the coverage
    requirementCoverage =
      if monotonicity then
        (subsetCoverage components combinable)+(justAddThese)+(numFailedAttackers) -- this formula is not very efficient, but it is more precise.
        --A less precise but more efficient would be to compute the subset percentage directly using
        --(logCoverage components combinable)+((fromIntegral (numFailedAttackers+jusAddThese))/(fromIntegral$ 2^(length components))
      else
        (fromIntegral (length minimalAttackers))+numFailedAttackers
    componentsInMinimals =
        if (length minimalAttackers) == 0 then
          Nothing
        else
          Just (((fromIntegral countComponentsInMinimals) /(fromIntegral $length minimalAttackers)))
    subsetPercentage =
        let
          res = if r_MaxSuccessful then fromRational (fromIntegral requirementCoverage)/(fromIntegral$ 2^(length components)) else 1
          (_,maxDoubleExp)= floatRange 0
        in
          if isNaN res then
             --In this case, haskell cannot fit the the requirement coverage ratio into a double. We do the efficient but less precise version of coverage.
             if monotonicity then
               (logCoverage components combinable)
              else
                (((fromIntegral $length minimalAttackers)+(fromIntegral numFailedAttackers))/(fromIntegral $ 2^(maxDoubleExp-1)))::Double
          else
            res
  in
    (
    --combinable,
    (fromIntegral requirementExpression)
    ,(r_MaxSuccessful)
    ,time
    ,(fromIntegral$ length components)
    ,(fromIntegral $ length minimalAttackers)
    ,(fromIntegral $ length attackerReports)
    --(fromIntegral $ 1+ (length minimalAttackers)+(fromIntegral numFailedAttackers))
    ,componentsInMinimals
    ,subsetPercentage)
      -- Result
      -- --combinable,
      -- (fromIntegral requirementExpression)
      -- (r_MaxSuccessful)
      -- time
      -- (fromIntegral$ length components)
      -- (fromIntegral $ length minimalAttackers)
      -- (fromIntegral $ length attackerReports)
      -- --(fromIntegral $ 1+ (length minimalAttackers)+(fromIntegral numFailedAttackers))
      -- componentsInMinimals
      -- subsetPercentage
      -- --,relativePercentage
      -- --,requirementCoverage
      -- --,relative

processAttacker::Text.Text->(Bool,[Integer])
processAttacker attackerReport =
  let
    --each attacker report starts with a letter that signals whether it is successful or not.
    (s:components)=
      let
       res = (Text.splitOn (Text.pack " ") attackerReport) -- use init to drop 0
      in
        if length res > 0 then
          init res
        else
          error ("The attacker report "++(Text.unpack attackerReport)++" is malformed.")
    successful = (Text.unpack s)=="S"
  in
    (successful,map (read.(Text.unpack))  components :: [Integer])

relativeCoverage ::(Eq a, Show a)=>[a]-> [[a]]-> Integer
relativeCoverage [] _ = 0
relativeCoverage _ [] = 0
relativeCoverage universe filters =
  let
    universeSize = fromIntegral (length universe)
  in
    if head filters == [] then
      combinedTo (fromIntegral $universeSize) (fromIntegral attackerLimit) --to use with subsets up to a size
    else
      let
        --this is a tricky one. We use inclusion-exclusion principle
        --combinations = init $ subsetsRestrictedUpToK filters attackerLimit-- discard the empty set.
        combinations = init $ subsetsUpToK filters attackerLimit-- discard the empty set.
        --computeArea = \s -> let subsetSize= ((length s)) in ((2^(universeSize-subsetSize))) --total area if using all subsets
        computeArea = \s -> let subsetSize= fromIntegral((length s)) in combinedTo (fromIntegral (universeSize - subsetSize)) (fromIntegral (attackerLimit-subsetSize))
      in
        --Debug.trace("groups: "++show (map show groupedCombinations))$
        coverageHelper computeArea combinations 0
          where
            coverageHelper _ [] acc     = acc
            coverageHelper computeArea (x:xs) acc =
              let
                set = nub . concat $x
                op = if odd (length x) then (+) else (-)
                -- each element of x is a set of sets. for each set of set, do union
                area = computeArea set
                -- now, x is a list itself, we compute the union
                acc' = acc `op` area
              in
              --Debug.trace("subset: "++show x)$ --The problem is that I am generating subsets that are too big! e.g. by combining 3 attackers of size 3.
              --Debug.trace("delta: "++ show (0 `op` area))$
                coverageHelper computeArea xs acc'

subsetCoverage ::(Eq a, Show a)=>[a]-> [[a]]-> Integer
subsetCoverage [] _ = 0
subsetCoverage _ [] = 0
subsetCoverage universe filters =
  let
    universeSize = fromIntegral (length universe)
    -- comparator = (\x y -> compare (length x) (length y))
    -- sortedFilters = sortBy comparator filters
  in
    --Debug.trace("combining: "++show filters)$
    if length filters <= 0 then
      error ("Pre of subsetCoverage does not hold")
    else
      if head filters == [] then
        --combinedTo (fromIntegral $universeSize) (fromIntegral attackerLimit) to use with subsets up to a size
        2^(universeSize) -- since the empty attacker successful, there is a coverage of 100% only if not using limits
      else
        let
          --this is a tricky one. We use inclusion-exclusion principle
          combinations = init $ subsets filters -- discard the empty set.
          --combinations = init $ subsetsRestrictedUpToK sortedFilters attackerLimit-- discard the empty set.
          computeArea = \s -> let subsetSize= ((length s)) in ((2^(universeSize-subsetSize))) --total area if using all subsets
          --computeArea = \s -> let subsetSize= ((length s)) in combinedTo (fromIntegral (universeSize - subsetSize)) (fromIntegral (attackerLimit-subsetSize))
        in
          --Debug.trace("groups: "++show (map show groupedCombinations))$
          coverageHelper computeArea combinations 0
            where
              coverageHelper _ [] acc     = acc
              coverageHelper computeArea (x:xs) acc =
                let
                  set = nub . concat $x
                  op = if odd (length x) then (+) else (-)
                  -- each element of x is a set of sets. for each set of set, do union
                  area = computeArea set
                  -- now, x is a list itself, we compute the union
                  acc' = acc `op` area
                in
                   --Debug.trace("subset: "++show set)$ --The problem is that I am generating subsets that are too big! e.g. by combining 3 attackers of size 3.
                  -- --Debug.trace("subsets: "++show sets)$
                --Debug.trace("delta: "++ show (0 `op` area))$
                  coverageHelper computeArea xs acc'

logCoverage ::(Eq a, Show a)=>[a]-> [[a]]-> Double
logCoverage [] _ = 0
logCoverage _ [] = 0
logCoverage universe filters =
  let
    universeSize = fromIntegral (length universe)
    -- comparator = (\x y -> compare (length x) (length y))
    -- sortedFilters = sortBy comparator filters
  in
    --Debug.trace("combining: "++show filters)$
    if head filters == [] then
      --combinedTo (fromIntegral $universeSize) (fromIntegral attackerLimit) to use with subsets up to a size
      1 -- since the empty attacker successful, there is a coverage of 100% only if not using limits
    else
      let
        --this is a tricky one. We use inclusion-exclusion principle
        combinations = init $ subsets filters -- discard the empty set.
        --combinations = init $ subsetsRestrictedUpToK sortedFilters attackerLimit-- discard the empty set.
        computeArea = \s -> let subsetSize= (fromIntegral(length s)) in (2**(-subsetSize)) --total area if using all subsets
        --computeArea = \s -> let subsetSize= ((length s)) in combinedTo (fromIntegral (universeSize - subsetSize)) (fromIntegral (attackerLimit-subsetSize))
      in
        --Debug.trace("groups: "++show (map show groupedCombinations))$
        coverageHelper computeArea combinations 0
          where
            coverageHelper _ [] acc     = acc
            coverageHelper computeArea (x:xs) acc =
              let
                set = nub . concat $x
                op = if odd (length x) then (+) else (-)
                -- each element of x is a set of sets. for each set of set, do union
                area = computeArea set
                -- now, x is a list itself, we compute the union
                acc' = acc `op` area
              in
                 --Debug.trace("subset: "++show set)$ --The problem is that I am generating subsets that are too big! e.g. by combining 3 attackers of size 3.
                -- --Debug.trace("subsets: "++show sets)$
              --Debug.trace("delta: "++ show (0 `op` area))$
                coverageHelper computeArea xs acc'

-- percentage ::(Eq a, Show a)=> [a]-> [[a]]-> Float
-- percentage [] _ = 1.0
-- percentage _ [] = 0.0
-- percentage universe filters =
--   let
--     comparator = (\x y -> compare (length x) (length y))
--     sortedFilters = sortBy comparator filters
--   in
--     if head sortedFilters == [] then
--       1.0 -- since the empty attacker successful, there is a percentage of 100%
--     else
--       --the
--       let
--         universeSize = fromIntegral (length universe)
--         --this is a tricky one. We use inclusion-exclusion principle
--         combinations = sortBy comparator $  subsetsUpToK sortedFilters 3-- discard the empty set. --unfortunately, using subsets is still TOO BIG
--         groupedCombinations =   tail $ groupBy (\x y -> (length x) == (length y)) combinations
--         --each group of combinations is ready to apply inclusion exclusion. We need to define the initial percentage values
--         --computeArea = \s -> let subsetSize= fromIntegral ((length s)) in (1/(2^subsetSize))
--         computeArea = \s -> let subsetSize= fromIntegral ((length s)) in (1/(2^subsetSize))
--       in
--         --Debug.trace("groups: "++show groupedCombinations)$
--         percentageHelper computeArea True groupedCombinations 0.0
--           where
--             percentageHelper _ _ [] acc     = acc
--             percentageHelper computeArea b (x:xs) acc =
--               let
--                 op = if b then (+) else (-)
--                 -- each element of x is a set of sets. for each set of set, do union
--                 sets = map (nub.concat) x
--                 areas = map computeArea sets
--                 -- now, x is a list itself, we compute the union
--                 area = foldl' op 0 areas -- we use foldl' because - is not associative and we want eager evaluation.
--                 acc' = acc + area
--               in
--                 -- Debug.trace("group: "++show sets)$
--                 -- Debug.trace("subsets: "++show sets)$
--                 --Debug.trace("acc: "++show acc')$
--                 percentageHelper computeArea (not b) xs acc'

choose :: [b] -> Int -> [[b]]
_      `choose` 0 = [[]]
[]     `choose` _ =  []
(x:xs) `choose` k =  (x:) `fmap` (xs `choose` (k-1)) ++ xs `choose` k

combined :: Integer -> Integer -> Integer
_      `combined` 0 =  1
0     `combined` _ =  0
n `combined` k =  ((n-1) `combined` (k-1)) + ((n-1) `combined` k)

combinedTo :: Integer -> Integer -> Integer
combinedTo n k = foldr (+) 0 (map (combined n) [0..k])

combinedToRestricted :: Integer -> Integer -> Integer-> Integer
combinedToRestricted n x k = foldr (+) 0 (map (combined (n-x)) [0..k-x])

subsetsUpToK:: [b] -> Integer -> [[b]]
subsetsUpToK _ 0 = [[]]
subsetsUpToK [] _ = [[]]
subsetsUpToK (x:xs) k =
  let
    withoutX= subsetsUpToK xs k
    forcingX= map (x:)(subsetsUpToK xs (k-1))
  in
    forcingX++withoutX

subsetsRestrictedUpToK:: (Eq b, Show b)=>[[b]] -> Int -> [[[b]]]
subsetsRestrictedUpToK _ 0 = [[]]
subsetsRestrictedUpToK [] _ = [[]]
subsetsRestrictedUpToK (x:xs) k =
  let
    withoutX= subsetsRestrictedUpToK xs k
    forcingX= map (x:)(subsetsRestrictedUpToK xs (k-1))
  in
    filter (\x-> length ((nub .concat) x)<= k) $ forcingX++withoutX

subsets::[b]-> [[b]]
subsets x = subsetsUpToK x (fromIntegral $ length x)

filterAttackers sampleSize attackers =
  let
    categories = Split.splitWhen (\x -> length x >2 )attackers
    --there should be two categories
  in
    if length categories >1 then
      (head categories)++(take sampleSize $head $ tail categories)
    else
      concat categories
  --we don't want too many attackers with 3 elements
