
module Main
(
  main
)
where
import System.Environment
import System.Exit
import Data.List
import Quantification

main = do
  args <- getArgs
  if args==[] then
    printUsage
  else
    case find (\x -> x=="-h" || x=="--help") args of
      Just _ -> printUsage
      Nothing ->
        do
          let
            timeout =
              case findIndex (=="-t") args of
                Nothing -> 600000
                Just x -> read (args!!(x+1))::Int
            attackers =
              case findIndex (=="-a") args of
                Nothing -> 20
                Just x -> read (args!!(x+1))::Int

            monotonicity =
              case findIndex (=="-m") args of
                Nothing -> True
                Just _ -> False

            benchmark = last args
          process monotonicity timeout attackers True True benchmark

printUsage = putStrLn "Usage: [-t Int] [-a Int] [-m] directory"
