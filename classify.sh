#!/bin/bash

#  classify.sh
#
#
#  Created by Eric Rothstein on 10/10/19.
#

#--------------------------------------------------------------------------#

run () {
  test $status =1 && return
  $*
  status=$?
}

aigtoaag () {
  echo ""
  echo "---------------------------------------------------------"
  echo "Processing AIG"
  echo "---------------------------------------------------------"
  file=$1
  if [ ${file: -4} == ".aig" ]
  then
    echo "Transforming aig into aag..."
    bm=$(basename $1 .aig)
    aiger/aigtoaig -a $1 >> $bm.aag
    rm -f aags/$bm.aag
    cp $bm.aag aags/$bm.aag
    rm $bm.aag
    aag=$bm.aag
  elif [ ${file: -4} == ".aag" ]
  then
    echo "Relocating aag..."
    bm=$(basename $1 .aag)
    rm -f aags/tmp
    cp $1 aags/tmp
    rm -f aags/$bm.aag
    cp aags/tmp ./aags/$bm.aag
    rm -f aags/tmp
    aag=$bm.aag
  else
    echo "the given file is not AIGER"
    exit 1
  fi
  echo "aag file now in aags folder."
}
#createsat $timesteps $components $timeout $attackers $isolation $monotonicity $clustering $bm.aag
createsat(){
#we call the classify script.
iflag=""
mflag=""
uflag=""
if [ $4 -eq 0 ]
then
    iflag="-i"
fi
if [ $5 -eq 0 ]
then
    mflag="-m"
fi
if [ $6 -eq 0 ]
then
    uflag="-u"
fi

cd parser
rm -rf $bm
./parseaig -k $1 -c $2 -t $3 $iflag $mflag $uflag ../$7
}


analyseResults(){
mflag=""

if [ $3 -eq 0 ]
then
    mflag="-m"
fi

cd analysis
./analyse -t $1 -a $2 -t $mflag $uflag $4 | tee ../$4-Results.txt
cd ..
}

#--------------------------------------------------------------------------#
timesteps=10
components=3
timeout=600000
attackers=20
isolation=1
monotonicity=1
clustering=1

echo "---------------------------------------------------------"
echo "AIG Attacker classification started"
echo "---------------------------------------------------------"

if [ $# -eq 0 ]
  then
    echo "No AIGER file supplied"
    exit 1
fi

while getopts "k:c:t:a:miu" opt; do
  case $opt in
    k)
      timesteps=$OPTARG
      echo "-k used. Bounding verification to $OPTARG time steps." >&2
      ;;
    c)
      components=$OPTARG
      echo "-c used. Restricting minimal attackers to max $OPTARG components." >&2
      ;;
    t)
      timeout=$OPTARG
      echo "-t used. Restricting verification time of each requirement to $OPTARG ms" >&2
      ;;
    a)
      attackers=$OPTARG
      echo "-a used. Using $OPTARG attackers to compute coverage." >&2
      ;;
    m)
    monotonicity=0
    echo "-m used. Monotonicity disabled."
    ;;
    i)
    isolation=0
    echo "-i used. Isolation disabled"
    ;;
    u)
    clustering=0
    echo "-u used. Clustering disabled."
    ;;
    h)
    echo "Usage: ./classify.sh [-k Int] [-c Int] [-t Int] [-a Int] [-m] [-i] [-u] aig"
    echo "Flags:"
    echo "-k: number of time steps for bounded model checking (default 10)"
    echo "-c: maximum number of components that minimal attackers are allowed to have (default 3)"
    echo "-t: maximum time for SAT solving, i.e., timeout, in ms (default 60000)"
    echo "-a: number of successful attackers used to compute the coverage (default 20)"
    echo "-m: disable monotonicity"
    echo "-i: disable isolation"
    echo "-u: disable clustering"
    echo ""
    echo "read the README.md for more information on these parameters."
    exit 0;
    ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

aigtoaag ${@: -1}
echo ""
echo "---------------------------------------------------------"
echo "Creating SAT Encodings "
echo "---------------------------------------------------------"

echo "Using "$bm #".aag"

echo "Bounding verification to $timesteps time steps."
echo "Restricting minimal attackers to max $components components."
echo "Restricting verification time of each requirement to $timeout ms"
echo "Using $attackers attackers to compute coverage."
echo "Isolation: $isolation"
echo "Monotonicity: $monotonicity"
echo "Clustering: $clustering"

createsat $timesteps $components $timeout $isolation $monotonicity $clustering aags/$bm.aag
echo ""
echo "---------------------------------------------------------"
echo "Compiling and Running SAT Encodings "
echo "---------------------------------------------------------"
cd $bm
./compile.sh
./run.sh
cd ../..

echo ""
echo "---------------------------------------------------------"
echo "Analysing results "
echo "---------------------------------------------------------"

analyseResults $timeout $attackers $monotonicity $bm
