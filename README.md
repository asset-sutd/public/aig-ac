# Attacker Classification using Bounded Model Checking
This readme guides you through the installation and use of the artefact to replicate the results of attacker classification for And-inverter Graphs (AIGs), presented in the submitted paper \[2\].

## System Requirements
The original experiments were carried out on a quad core MacBook with 2.9GHz Intel Core i7 and 16GB RAM. It is possible that with less memory or a slower proccessor some experiments may take longer or fail partially, producing slightly different results. We recommend that you enable a swap file of at least 16GB, if possible. 

## Software requirements
The tool mainly relies on the following software:
* Haskell platform (to obtain it, see https://www.haskell.org/platform/)
* AIGER (to obtain it, see http://fmv.jku.at/aiger/)
* CaDiCal Simplified Satisfiability Solver (to obtain it, see http://fmv.jku.at/cadical/) 

**Make sure you configure CaDiCal to work with clang, using  ./configure CXX=clang++ during its installation**

## Structure of the Tool
This tool has 4 modules:
* A module for the handling of AIGER files (`aiger`)
* A module for SAT solving (`cadical`)
* A module that encodes the problem of attacker classification (`parser`)
* A module that analyses the results of attacker classification to compute measures (`analysis`)

## Installation
To install the tool, run 
```
chmod +x install.sh
./install.sh 
```
You will be prompted for the root password to install Haskell's ghc. 

## Test
To test the tool, you can run the script 
```
./classify.sh test/test.aig
```
You should see the following results after execution, which is also saved in the file `test-Results.txt`:
```
---------------------------------------------------------
Analysing results 
---------------------------------------------------------
Req.    Time    Comp.    #Suc.    #SAT    Av. Comp. per Suc.    Cov.
89    0    10    1    2    0.0    1.0
99    1    10    5    32    1.0    0.994140625
105    1    10    5    32    1.0    0.994140625
107    1    10    5    32    1.0    0.994140625
113    3    10    5    32    1.0    0.994140625
115    1    10    5    32    1.0    0.994140625
117    1    10    5    32    1.0    0.994140625
121    1    10    5    32    1.0    0.994140625
123    1    10    5    32    1.0    0.994140625
125    1    10    5    32    1.0    0.994140625
127    1    10    5    32    1.0    0.994140625
P(OoM)    Av.Time    Av.Comp.    Av.#Suc.    Av.#SAT    Av.Comp. per Suc.    Av.Cov.
0.0    1.0909090909090908    10.0    4.636363636363637    29.272727272727273    0.9090909090909091    0.9946732954545454
```
**Be aware that the labels may not align with the numbers because we do not trim decimals. These results are best visualised in a spreadsheet app (e.g. Microsoft Excel or Libreoffice Calc).**

## The classify.sh Script
This section describes the `classify.sh` script, which describes the process of attacker classification. 

The process of attacker classification has four phases:
1. Obtaining an `.agg` file from an AIGER file using the tools in the `aiger` module
2. Creating a set of `.cpp` files that encode the SAT problems using the tools in the `parser` module
3. Compiled and execute the resulting programs to perform SAT solving using the `cadical` module
4. Analyse the results of SAT solving and compute the coverage and other measures using the `analysis` module

The `classify.sh` script goes through all phases. To check the usage of `classify.sh`, you can invoke it with the parameter `-h`.

### Inputs
The `classify.sh` script must be given the following parameters:
* An AIG file `S` in AIGER format (see \[1\]);
* The number of time steps to consider (flag `−k`). The default value is 10;
* The number of maximum elements that minimal attackers may control (flag
`−c`). The default is 3;
* The timeout value in milliseconds for the analysis of each requirement (flag
`−t`). The default is 60000ms;
* The number of minimal attackers to consider for the computation of the
coverage (flag `−a`). The default is 20.  The more you select, the less conservative the coverage estimation is (i.e., coverage may slightly increase because it is considering more attackers), but it may take exponentially longer to compute, since we account for overlaps between attackers.

Additionally, the script can be given the following optional parameters:
* The flag `−m` which disables monotonicity;
* The flag `−i` which disables isolation;
* The flag `−u` which disables clustering.

### Outputs
When finished, the `classify.sh` script prints on the screen the following information for each requirement `r = G e (globally e)`. 
* The associated requirement expression `e` in AIGER format - `Req.`;
* The classification time in ms - `Time` ;
* The number of components that are not isolated from `r` (i.e., components that are in the Cone-of-Influence of the requirement) - `Comp.`;
* The number of successful attackers that were identified - `#Suc.`;
* The number of calls to the SAT solver - `#SAT`;
* The average number of components per successful attacker - `Av. Comp. per Suc.`;
* The coverage for this requirement - `Cov.`.

Additionally, it prints a set of average measures:
* The probability of a requirement to run out of memory during SAT solving - `P(OoM)`;
* The average classification time of a requirement in ms - `Av.Time` ;
* The average number of components that are not isolated per requirement - `Comp.`;
* The average number of successful attackers per requirement - `Av.#Suc.`;
* The average number of calls to the SAT solver per requirement - `#SAT`;
* The average number of components per successful attacker per requirement- `Av. Comp. per Suc.`;
* The coverage per requirement - `Av. Cov.`.

### Computational Side-effects
The artefact has the following computational side-effects:
* a folder for the benchmark `S`, located in the `parser` folder, where it places:
  * a set of `.cpp` files, one per requirement cluster
  * a set of `.o` files and executables, which results from the compilation of
the above .cpp files
  * a set of `.txt` log files that contain information about the calls made to
the SAT solver. 
* the file `SResults.txt`, located in the same directory as the `./classify.sh` script that records the outputs of the classification for future reference.

## Replicating Results
For this section, we really recommend having a swap file of at least 16GB, otherwise results may vary since the SAT solver runs out of memory too soon when compared to our experimental setup.

### Replicating Section 5.1 of \[2\]
We now show how we created the data reported in Figures 3 and 4. Depending on the size of the AIG, it may take quite long to compute the results, and it takes even longer when monotonicity and isolation are disabled. The following are time estimates for the classification with default values for the different benchmarks that we evaluated in the paper (i.e., left: without disabling monotonicity and without disabling isolation; right: disabling monotonicity and isolation): 

- `pdtvsarmultip` : about 15 minutes   -- 1.5 **hours** 
- `6s106`         : about 1 minute     -- 1.7 **hours**
- `6s155`         : about 5 **hours**  -- 16 **hours**
- `6s255`         : about 15 minutes   -- 3 **hours**
- `6s325`         : about 15 **hours** -- 22 **hours**
- `bob12m18m`     : about 2 **hours**  -- 25 **hours**
- `nusmvdme2d3multi`: less than 1 minute -- 1 minute

We encourage you to reproduce all the results for the benchmarks `pdtvsarmultip`, `6s106`, `6s255`, and `nusmvdme2d3multi` which respectively take, in total, about 2 **hours**, 2 **hours**, 5 **hours** and 2 minutes. The other benchmarks take too much time for total replication (from 15 to 22 **hours**). In case you want to partially replicate the results for the other benchmarks, we recommend to focus on replicating results without disabling monotonicity or isolation (i.e., by calling `./classify.hs` with those benchmarks).

We provide the following scripts for convenience:
```
./replicatepdtvsarmultip.sh
./replicate6s106.sh
./replicate6s255.sh
./replicatenusmvdme2d3multi.sh
```
We encourage you to check those scripts if you want to perform your own variations of the experiments. In a nutshell, they create a copy of the AIGER file and invoke the classifier with different flags.

**IMPORTANT**: we do not remove the side-effect files automatically, in case you want to check them out. They are in the folders placed inside the `parser` folder, named after the benchmarks, and they can add up in space rather quickly. They can be removed after executing the analysis.

### Replicating Section 5.2  of \[2\]
The script
```
./replicateTables123.sh
```
computes the values used in Tables 1, 2 and 3 of the paper as part of a larger set of data (i.e., what we reported in the tables are the averages, but the individual values are also computed here). This script should take about 1 hour to execute.

## Classifying more AIGs
In the folder `hwmcc13/multi/` we have included the set of benchmarks that was used in the Hardware Model Checking competition in 2013 (see http://fmv.jku.at/hwmcc13/index.html), in case you want to classify attackers with other benchmarks beyond the ones mentioned in this Readme.

## References
1. Biere, A., Heljanko, K., Wieringa, S.: AIGER 1.9 and beyond. Tech. rep., FMV Reports Series, Institute for Formal Models and Verification, Johannes Kepler Uni- versity, Altenbergerstr. 69, 4040 Linz, Austria (2011)
2. Rothstein-Morris, E., Jun, S., Chattopadhyay, S.: Systematic classication of attackers via bounded model checking. Submitted to VMCAI 2020
