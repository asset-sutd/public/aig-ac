module AIGtoCPP (
Params(..),
process
)
where
import qualified Data.ByteString as Str
import Data.Binary.Get
import Data.Binary.Put
import Data.Array
import qualified Data.List.Split as Split
import Control.Monad
import qualified System.Directory as Directory
import System.IO
import System.Environment
import System.Directory
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import qualified Debug.Trace as Debug
import Data.List

-- This parser takes .aig file <example>.aig and creates a Haskell script <example>.hs that works as a library for model checking

type Header = Map.Map HeaderToken Int
data HeaderToken = M | I | L | O | A | B | C deriving (Ord,Show,Eq)
data TemplateToken = MODULE_NAME |  INPUT_ARRAY | INPUT_BINDING | STATE_ARRAY | STATE_PRIME_ARRAY | TRANSITION_RULES | INITIAL_STATE | BAD_VARIABLE_BINDING| BAD_VARIABLE_ARRAY deriving (Ord,Eq,Show)

--We consider two numberings: an AIGER numbering (e.g. 0 is false, 1 is true, 2 refers to variable 1 and 3 refers to the negation of variable 1), and we have a variable numbering 1,2,3,4,5,6
data Variable = Var Int deriving (Eq, Ord)
data Expression = Exp Int deriving (Eq, Ord)
data Component = Input Variable | Gate Variable Expression Expression | Latch Variable Expression Expression | BadVariable Expression | Output Expression deriving (Ord,Eq)
{-Semantics are as follows:
  Input i         : represents v_i(k) = input_i(k)
  Gate i exp1 exp2: represents v_i(k) = exp1(k) && exp2(k), where exp(k) is an expression at time k. (expressions are numbers)
  Latch i exp b   : represents l_i(0)= b and l_i(k) = exp(k-1)
  BadVariable exp : represents assert (not exp)
  Output exp      : represents assert (exp)
-}
data Edge = Edge Int Int deriving (Show,Ord,Eq)

data TarjanState a = TarjanState {indexMap::Map.Map a Int, lowlinkMap::Map.Map a Int, onStack::Set.Set a, index::Int, stack::[a], sccs::[SCC a], sccMap:: Map.Map a (SCC a)  } deriving (Show,Eq,Ord)
data Graph a = Graph {vertices::(Set.Set a), predecessors::(Map.Map a (Set.Set a)), successors::(Map.Map a (Set.Set a))} deriving (Show,Ord,Eq)
data AIG = AIG {numVariables::Int, inputs::[Component], latches::[Component], outputs::[Component],gates::[Component], description::Map.Map Int Component} deriving (Show,Ord,Eq)
type SCC a = [a]
type Equation = ((Either Variable Expression, Int), Either (Maybe Bool) [(Expression,Int)]) --An equation has (i,t)::(Int,Int) to describe the lhs v_i(t) and has [(e,t')] to describe the rhs e(t').

jaccard = 100 --unfortunately, if the Jaccard index is less than 100, then we will start to check non=interference attackers for some requirements.

{-
  Equation Semantics
  An equation can be of the form
    o(t)= exp(t)              for outputs
    v(0)= bool                for latches
    v(t+1)= exp(t)            for latches
    v(t)= exp1(t) && exp2(t)  for gates
    i(t)= Nothing             for inputs (Nothing means "depends on external input" i.e. currently undefined), so an equation ((Left Var 2, 3),Nothing) Means i_2(3) is a "source literal" in the CNF.
-}

instance Show Variable where
  show (Var i) = "v"++show i

instance Show Component where
  show (Input v@(Var i))= (show v)++"(t) = i"++(show i)++"(t)"
  show (Gate v e1 e2 )  = (show v)++"(t) = ("++(show e1)++")(t) && ("++ (show e2)++")(t)"
  show (Latch v e b)    = (show v)++"(0) = "++(show b)++" and "++(show v)++"(t+1) = ("++(show e)++")(t)"
  show (BadVariable e)  = "assert("++(show e)++")"
  show (Output e)       = "assert("++(show e)++")"

getVariable::Component->Int
getVariable c =
  case c of
    Input (Var i)       -> i
    Gate (Var i) _ _    -> i
    Latch (Var i) _ _   -> i
    Output (Exp e)    -> e `div` 2

getRHSVariables::Equation->[(Either Variable Expression, Int)]
getRHSVariables (_,rhs) =
  case rhs of
    Left _ -> [] -- no variables on boolean values or inputs
    Right exps -> map (\(Exp e,k) -> (Left $ Var $ e`div`2,k)) exps

  -- case c of
  --   Latch _ (Exp e) _                -> [e `div` 2]
  --   Gate _ (Exp e1) (Exp e2)         -> nub [(e1 `div` 2),(e2 `div`2)]
  --   (BadVariable (Exp e))            -> [e `div` 2]
  --   (Output (Exp e))                 -> [e `div` 2]
  --   otherwise -> error("Component "++show c++" does not have an associated rhs variable index because it is an input." )

instance Show Expression where
  show (Exp e)
    | e == 0 = "False"
    | e == 1 = "True"
    | even e =
      let i = e `div` 2 in
          "v"++(show i)
    | odd e =
      let i = (e-1) `div` 2 in
          "(not (v"++(show i)++"))"

data Params = Params{fileName::[Char], k::Int, maxAttackers::Int, clustering::Bool, isolation::Bool, monotonicity::Bool, timeout::Int} deriving (Eq,Show)

defaultParams = Params "" 10 3 True True True 600000

process:: Params -> IO ()
process (Params fileName k maxAttackers clustering isolation monotonicity timeout) =
  do

    contents <- readFile fileName--readFile ("aags/"++fileName++".aag") --openFile ("test/aags/"++fileName++".aag") ReadMode
    cadicalTemplate <-
      if monotonicity then
        readFile ("./CadicalTemplate.cpp")
      else --if (not monotonicity) && isolation then
        readFile ("./CadicalTemplateWOMO.cpp")
          -- else if (monotonicity) && (not isolation) then
          --     readFile ("CadicalTemplateWONI.cpp")
          --   else
          --     readFile ("CadicalTemplateWONIMO.cpp")
    compileTemplate <- readFile ("./CompileTemplate.sh")
    runTemplate <- readFile ("./RunTemplate.sh")
    --h_out <- openFile ("test/Mod"++fileName++".hs") WriteMode
    let
      aig@(AIG numVariables numInputs latches outputs gates description) = buildAIG (contents)
      --graph@(Graph variables predecessors successors) = buildGraph numVariables latches gates --This graph does not span over time.
      (coi,ioc) = buildCones aig -- ::(Int -> [Int], Int -> [Int])
      --We can now compute the Jaccard index to group similar requirements together
      -- for each output, I want to create a system of equations. This is what I will write in the cpp
      -- Ultimately, for each output o, we want the unwinding of the equation o(k). This yields a system of equations:
      equations = foldr (enhanceEquationMap aig k) (Map.empty) outputs
      -- from those equations, we are interested in all those that are relevant for the different outputs. Let us map over outputs, selecting those that are related to it.
      sortedOutputs = sortBy (\o1 o2 -> (coi (getVariable o1)) `compare` (coi (getVariable o2))) outputs
      -- for DUMB groupedOutputs = map (:[]) sortedOutputs
      groupedOutputs =
        if isolation then
          if clustering then
            if jaccard /=100 then
              (groupBy (\o1 o2 -> jaccard*((length ((coi (getVariable o1)) `union` (coi (getVariable o2)))))<=100*(length ((coi (getVariable o1)) `intersect` (coi (getVariable o2))))) sortedOutputs)
            else
              groupBy  (\o1 o2 -> (coi (getVariable o1)) == (coi (getVariable o2))) sortedOutputs
          else
              map (:[]) sortedOutputs
        else
          groupBy  (\o1 o2 -> True) sortedOutputs
      requirements = map (\group ->map (\(Output o) -> (Map.toList $ Map.filterWithKey (\(lhs,k) rhs -> case lhs of Right e -> e==o ; otherwise -> False) equations)) group) groupedOutputs
      --Note that we mapped over outputs, we should be able to then zip with the respective output
      --requirements is a list of lists. For each requirement, we want to produce a SET of the relevant encodings that will create the CNF.
      --We need to explore the map, queueing RHSs. Alternatively,  keys of the equations are often Left variables
      lito = \o -> case findIndex (o==) outputs of Just i -> (1+i)
      simpleClusters = map (\cluster -> map (\o@(Output (Exp e)) -> (e,lito o)) cluster ) groupedOutputs
      theCNF = map (getCNF k aig equations) requirements
      systems = zip simpleClusters $ theCNF
      (filesData,clusterNames) =
        if isolation then
          (map (fillCadicalTemplate cadicalTemplate aig k maxAttackers timeout) systems,map (\n -> "cluster"++show n) [1..length groupedOutputs])
        else
          let
            trueOutputs =
              if clustering then
                if jaccard /=100 then
                  (groupBy (\o1 o2 -> jaccard*((length ((coi (getVariable o1)) `union` (coi (getVariable o2)))))<=100*(length ((coi (getVariable o1)) `intersect` (coi (getVariable o2))))) sortedOutputs)
                else
                  groupBy  (\o1 o2 -> (coi (getVariable o1)) == (coi (getVariable o2))) sortedOutputs
              else
                map (:[]) sortedOutputs
            trueRequirements = map (\group ->map (\(Output o) -> (Map.toList $ Map.filterWithKey (\(lhs,k) rhs -> case lhs of Right e -> e==o ; otherwise -> False) equations)) group) trueOutputs
            --Note that we mapped over outputs, we should be able to then zip with the respective output
            --requirements is a list of lists. For each requirement, we want to produce a SET of the relevant encodings that will create the CNF.
            --We need to explore the map, queueing RHSs. Alternatively,  keys of the equations are often Left variables
            trueClusters = map (\cluster -> map (\o@(Output (Exp e)) -> (e,lito o)) cluster ) trueOutputs
            trueNames = map (\n -> "cluster"++show n  ) [1..length trueOutputs]
            trueSystems = zip trueClusters $ (repeat (head $ theCNF))
          in
            (map (fillCadicalTemplateisolation cadicalTemplate aig k maxAttackers timeout (head theCNF)) trueSystems, trueNames)
      --clusterNames = map (\cluster -> concat $ map (\(Output (Exp e)) -> (show e)++"_") cluster ) groupedOutputs
    --putStrLn (show systems)
    let
      benchmark =
        let
          fn = Split.splitOn "/" fileName
        in
          if length fn == 1 then
            concat $ init $ Split.splitOn "." $ (head fn)
          else
            concat $ init $ Split.splitOn "." $ (last fn)
      benchmarkName = benchmark
        -- case (isolation,monotonicity) of
        --   (True,True) ->   benchmark
        --   (False,True) -> benchmark++"WONI"
        --   (True,False) -> benchmark++"WOMO"
        --   (False,False) ->  benchmark++"WONIMO"
    mapM_ (createCadicalFile benchmarkName) (zip clusterNames filesData)
    createCompileScript compileTemplate (benchmarkName) clusterNames
    createRunScript runTemplate (benchmarkName) clusterNames
    let
      directory =  benchmarkName++"/"--"cadical/"++benchmarkName++"/"
      name = (directory++benchmarkName++".aag")
    benchmark_out <- openFile name WriteMode
    hPutStr benchmark_out contents
    hClose benchmark_out
    info_out <- openFile (directory++"info.txt") WriteMode
    hPutStr info_out ("Time steps: "++(show k)++"\nMax # attacker components: "++(show maxAttackers)++"\nClustering: "++show clustering++"\nNon-interference: "++show isolation++"\nMonotonicity: "++show monotonicity++"\n")
    hClose info_out
    putStrLn $ "Processed " ++ (show $length outputs) ++" requirements"
    putStrLn "Done!"
    return ()

createCompileScript::String->String->[String]->IO()
createCompileScript template benchmark clusterNames =
  do
    let
      directory =  benchmark--"cadical/"++benchmark
      name = (directory++"/compile.sh")
      compilations = map (\c -> "compile "++benchmark++"-"++c++"\n") clusterNames
      runs = map (\c -> "run "++benchmark++"-"++c++"\n") clusterNames
      (templateHeader:rest0) = Split.splitOn "COMPILES_TEMPLATE" template
      rest = (concat rest0)
      compile_contents = templateHeader++(concat compilations)++rest
    compile_out <- openFile name WriteMode
    hPutStr compile_out compile_contents
    hClose compile_out
    currentPermissions <- (getPermissions name)
    setPermissions name (currentPermissions{executable = True})

createRunScript::String->String->[String]->IO()
createRunScript template benchmark clusterNames =
  do
    let
      directory =  benchmark-- "cadical/"++benchmark
      name = (directory++"/run.sh")
      runs = map (\c -> "run "++benchmark++"-"++c++"\n") clusterNames
      (templateHeader:rest0) = Split.splitOn "RUNS_TEMPLATE" template
      rest = (concat rest0)
      compile_contents = templateHeader++(concat runs)++rest
      run_contents = templateHeader++(concat runs)++rest
    run_out <- openFile name WriteMode
    hPutStr run_out run_contents
    hClose run_out
    currentPermissions <- (getPermissions name)
    setPermissions name (currentPermissions{executable = True}) --I just learned this to update the field of a data structure

--createCadicalFile::String->(Component,String)->IO()
createCadicalFile::String->(String,String)->IO()
createCadicalFile benchmark (name,contents) =
  do
    let
      directory =  "./"++benchmark --"cadical/"++benchmark
    Directory.createDirectoryIfMissing False directory
    --Directory.createDirectory directory
    h_out <- openFile (directory++"/"++benchmark++"-"++name++".cpp") WriteMode
    hPutStr h_out contents
    hClose h_out

fillCadicalTemplate::String->AIG->Int->Int->Int->([(Int,Int)],([(Int,Int)],[String])) -> String
fillCadicalTemplate template aig@(AIG numVariables numInputs latches outputs gates description) k maxAttackers timeout (requirements,(attackerInfo,systemInfo)) =
  let
    (cs,as) = unzip attackerInfo--
    attackers= concat $ map (\lit -> " "++(show lit)++",") $ as --vector of int that contains the attacker literals.
    components = concat $ map (\lit -> " "++(show lit)++",") $ cs --vector of int that contains the attacker literals.
    reqs = concat $ map (\(r,o) -> " "++(show r)++",") $ requirements
    numReqs = length requirements
    req2literal = map (\c -> case c of '(' -> '{'; ')'-> '}'; otherwise -> c ) $concat $ map (\lit -> " "++(show lit)++",") $ requirements
    --attacker2Component = map (\c -> case c of '(' -> '{'; ')'-> '}'; otherwise -> c ) $concat $ map (\lit -> " "++(show lit)++",") $ zip (map (getAttackerFlag numVariables k) attackerInfo) attackerInfo
    --actions = concat $ map (\lit -> " "++(show lit)++",") attackerActions --vector of int that contains the attacker action literals (attackerActionxtime).
    inputs = undefined --vector of int that contains the input literals (inputsXtime)
    --variables = undefined --vector of int that contains the variable literals. these NOT NEEDED (only interested in system inputs and attacker actions)
    --requirements = undefined --vector of int that contains the requirement literals. NOT NEEDED (only one req per template)
    attackersMap = undefined -- each attacker has its attacker literal mapped to its variable. i.e. from attacker guard literal to component. ::Map Literal Var
    attackerActionsMap = undefined -- from attacker action literal to (var,t) ::Map Literal (Var, Time)
    inputsMap = undefined -- from input literal to (var,t) ::Map Literal (Var, Time)
    --no need to map
    system = concat (map (++"\n") systemInfo)
    timeoutString = "const double timeout = "++(show timeout)++";\n"
    attackerInfoString = "const int numComponents = "++(show (length attackerInfo))++";\n"
    numReqsString = "const int numRequirements = "++(show numReqs)++";\n"
    attackersString = if length attackerInfo > 0 then "const int attackers["++(show $ length attackerInfo)++"] ={"++(init attackers)++"};\n" else "const int attackers[0]={};\n"
    componentsString = if length attackerInfo > 0 then "const int components["++(show $ length attackerInfo)++"] ={"++(init components)++"};\n" else "const int components[0]={};\n"
    maxAttackersString = "const int maxFailComponents = "++(show maxAttackers)++";\n"
    reqsString = "const int requirements["++show numReqs++"]={"++(init reqs)++"};\n"
    req2LiteralString = "map<int,int> requirementToLiteral={"++(init req2literal)++"};\n"
    --attacker2ComponentString = if length attackerInfo > 0 then "map<int,int> attackerLiteralToComponent={"++(init attacker2Component)++"};\n" else "map<int,int> attackerLiteralToComponent;\n"
    --actionsString = if length attackerActions > 0 then "int["++(show $ length attackerActions)++"] actions ={"++(init actions)++"};\n" else "int[0] actions;\n"
    (templateHeader:rest) = Split.splitOn "TEMPLATE_HEADER" template
    --(declNumReqs:rest) = Split.splitOn "TEMPLATE_NUM_REQUIREMENTS" (concat rest0)
    -- (_:rest2)  = Split.splitOn "TEMPLATE_LITERALS" (concat rest1)
    -- (_:rest3)  = Split.splitOn "TEMPLATE_INDICES" (concat rest2)
    -- (_:rest4)  = Split.splitOn "TEMPLATE_REQUIREMENTS" (concat rest3)
    -- (_:rest5)  = Split.splitOn "TEMPLATE_NUM_MAX_ATTACKERS" (concat rest4)
    (beforeSystem:afterSystem)  = Split.splitOn "TEMPLATE_SYSTEM_DESCRIPTION" (concat rest)
  in
    (templateHeader++
    timeoutString++
    attackerInfoString++
    numReqsString++
    --declNumReqs++
    (attackersString)++
    componentsString++
    reqsString++
    req2LiteralString++
    maxAttackersString++beforeSystem++system++(concat afterSystem))

fillCadicalTemplateisolation::String->AIG->Int->Int->Int->(([(Int,Int)],[String]))->([(Int,Int)],([(Int,Int)],[String])) -> String
fillCadicalTemplateisolation template aig@(AIG numVariables numInputs latches outputs gates description) k maxAttackers timeout ((attackerInfo,systemInfo)) (requirements,(trueAttackerInfo,trueSystemInfo)) =
  let
    (cs,as) = unzip attackerInfo--
    attackers= concat $ map (\lit -> " "++(show lit)++",") $ as --vector of int that contains the attacker literals.
    components = concat $ map (\lit -> " "++(show lit)++",") $ cs --vector of int that contains the attacker literals.
    reqs = concat $ map (\(r,o) -> " "++(show r)++",") $ requirements
    numReqs = length requirements
    req2literal = map (\c -> case c of '(' -> '{'; ')'-> '}'; otherwise -> c ) $concat $ map (\lit -> " "++(show lit)++",") $ requirements
    --attacker2Component = map (\c -> case c of '(' -> '{'; ')'-> '}'; otherwise -> c ) $concat $ map (\lit -> " "++(show lit)++",") $ zip (map (getAttackerFlag numVariables k) attackerInfo) attackerInfo
    --actions = concat $ map (\lit -> " "++(show lit)++",") attackerActions --vector of int that contains the attacker action literals (attackerActionxtime).
    inputs = undefined --vector of int that contains the input literals (inputsXtime)
    --variables = undefined --vector of int that contains the variable literals. these NOT NEEDED (only interested in system inputs and attacker actions)
    --requirements = undefined --vector of int that contains the requirement literals. NOT NEEDED (only one req per template)
    attackersMap = undefined -- each attacker has its attacker literal mapped to its variable. i.e. from attacker guard literal to component. ::Map Literal Var
    attackerActionsMap = undefined -- from attacker action literal to (var,t) ::Map Literal (Var, Time)
    inputsMap = undefined -- from input literal to (var,t) ::Map Literal (Var, Time)
    --no need to map
    system = concat (map (++"\n") systemInfo)
    timeoutString = "const double timeout = "++(show timeout)++";\n"
    attackerInfoString = "const int numComponents = "++(show (length attackerInfo))++";\n"
    numReqsString = "const int numRequirements = "++(show numReqs)++";\n"
    attackersString = if length attackerInfo > 0 then "const int attackers["++(show $ length attackerInfo)++"] ={"++(init attackers)++"};\n" else "const int attackers[0]={};\n"
    componentsString = if length attackerInfo > 0 then "const int components["++(show $ length attackerInfo)++"] ={"++(init components)++"};\n" else "const int components[0]={};\n"
    maxAttackersString = "const int maxFailComponents = "++(show maxAttackers)++";\n"
    reqsString = "const int requirements["++show numReqs++"]={"++(init reqs)++"};\n"
    req2LiteralString = "map<int,int> requirementToLiteral={"++(init req2literal)++"};\n"
    --attacker2ComponentString = if length attackerInfo > 0 then "map<int,int> attackerLiteralToComponent={"++(init attacker2Component)++"};\n" else "map<int,int> attackerLiteralToComponent;\n"
    --actionsString = if length attackerActions > 0 then "int["++(show $ length attackerActions)++"] actions ={"++(init actions)++"};\n" else "int[0] actions;\n"
    (templateHeader:rest) = Split.splitOn "TEMPLATE_HEADER" template
    --(declNumReqs:rest) = Split.splitOn "TEMPLATE_NUM_REQUIREMENTS" (concat rest0)
    -- (_:rest2)  = Split.splitOn "TEMPLATE_LITERALS" (concat rest1)
    -- (_:rest3)  = Split.splitOn "TEMPLATE_INDICES" (concat rest2)
    -- (_:rest4)  = Split.splitOn "TEMPLATE_REQUIREMENTS" (concat rest3)
    -- (_:rest5)  = Split.splitOn "TEMPLATE_NUM_MAX_ATTACKERS" (concat rest4)
    (beforeSystem:afterSystem)  = Split.splitOn "TEMPLATE_SYSTEM_DESCRIPTION" (concat rest)
  in
    (templateHeader++
    timeoutString++
    attackerInfoString++
    numReqsString++
    --declNumReqs++
    (attackersString)++
    componentsString++
    reqsString++
    req2LiteralString++
    maxAttackersString++beforeSystem++system++(concat afterSystem))


getCNF::Int
     -> AIG
     -> Map.Map
          (Either Variable Expression, Int)
          (Either (Maybe Bool) [(Expression, Int)])
     -> [[Equation]]
     -- -> ([String],[String])
     -> ([(Int,Int)],[String])
getCNF k aig@(AIG numVariables numInputs latches outputs gates description) equations cluster =
  --initialization
  let
    --(attks,clauses_0) = unzip $ map (tseitinEncoding k aig) system
    encoding = map (\group -> snd $ unzip $ map (tseitinEncoding k aig) group ) cluster
    attackers_0 = Set.empty
    cnf_0 = foldr (\g acc-> Set.insert g acc) Set.empty (map (\group -> (concat$concat group)++"solver->add(0);;") encoding)
    visited = Set.empty
    queue = concat $ map getRHSVariables $ concat cluster --get the rhs variables of the system we want to solve
    (attackers',cnf',visited',queue')= getCNFLoop (attackers_0,cnf_0,visited,queue)
  in
    --Debug.trace("CNF_0 for"++show(queue)++" is "++show cnf_0)$
    (Set.toList attackers', Set.toList (cnf'))
      where
          getCNFLoop::(Set.Set (Int,Int),Set.Set String, Set.Set (Either Variable Expression, Int), [(Either Variable Expression, Int)])-> (Set.Set (Int,Int), Set.Set String, Set.Set (Either Variable Expression, Int), [(Either Variable Expression, Int)])
          getCNFLoop (attackers,cnf,visited,queue) =
            case queue of
              [] -> (attackers,cnf,visited,queue)
              (lhs@((Left v@(Var i)),t):rest) ->
              --have we visited this lhs?
                if Set.member lhs visited then
                  getCNFLoop (attackers,cnf,visited,rest)
                else
                  --get the equation for this variable. It should be easy
                  case Map.lookup lhs equations of
                    Nothing -> error ("There is no equation for variable "++show v++" at time "++ show t++" in the equations map.")
                    Just rhs ->
                      let
                        equation = (lhs,rhs)
                        visited' = Set.insert lhs visited
                        (attack,clauses)=(tseitinEncoding k aig equation)
                        attackers' =
                          case attack of
                            Nothing -> attackers
                            Just (i,flag) -> Set.insert (i,flag) attackers
                        cnf' = foldr (\clause cnfs-> Set.insert clause cnfs) cnf clauses
                        queue' = (getRHSVariables equation)++rest
                      in
                        --Debug.trace("found "++(show attks)++" for equation "++show (equation))$
                        getCNFLoop (attackers',cnf',visited',queue')

--This method takes an AIG A, an equations map E and an output o, and puts all the equations corresponding to o in E to return the new state of E
--The reason why this map changes with k is that information takes time to propagate from latch to latch, transitively.
enhanceEquationMap::
  AIG->
  Int ->
  Component ->
  Map.Map (Either Variable Expression, Int) ((Either (Maybe Bool) [(Expression,Int)])) ->
  Map.Map (Either Variable Expression, Int) (Either (Maybe Bool) [(Expression,Int)])
enhanceEquationMap aig@(AIG numVariables numInputs latches outputs gates description) k (Output o@(Exp e)) equations =
  --Have I checked this output before?
    case Map.lookup (Right o,k) equations of
      Just _  -> error("Why are you checking this output again? -> "++(show o))--equations
      Nothing ->
        let
          equations' =  foldr (\k' eqs-> Map.insert (Right o,k') (Right [(o,k')]) eqs) equations [0..k]-- we resolve the rhs of the output as an expression at time t.
          queue = map (\t-> (Left $ Var (e `div` 2),t)) [0..k] -- the rhs of an output at time t refers to a variable at time t. We are checking for outputs from t=0..k
        in
          enhanceHelper queue equations' -- the queue contains only LHS that need to be resolved. The map stores the RHSs
            where
              enhanceHelper queue equations =
                -- Debug.trace("queue: "++show (queue))$
                -- Debug.trace("equations: "++show (equations))$
                -- Debug.trace("------")$
                case queue of
                  []           -> equations
                  lhs@(Left (Var v),t):rest ->
                    case Map.lookup lhs equations of
                      Just _  ->
                        --Debug.trace("You already included equation"++show lhs)$
                        enhanceHelper rest equations
                      Nothing ->
                        --the equation for e(t) needs to be resolved, i.e., we need to chase down the AIG and time to reach a "base term"
                          if v == 0 then
                            let
                              (lhs',rhs') = (lhs,Left (Just False))
                              equations'= Map.insert lhs' rhs' equations
                            in
                              enhanceHelper rest equations'
                          else
                            let
                              -- we are dealing with something that's not an output. We first need to identify the component.
                              component =
                                case Map.lookup v description of
                                  Nothing ->
                                    error ("Cannot find a component for variable id "++show v)
                                  Just c -> c
                              -- from the component, we can obtain an equation, given a timestep
                              equation@(lhs',rhs') = toEquation t component
                              --in theory assert (lhs==lhs')
                            in
                              --Debug.trace("enhancing with "++show lhs'++" = "++show rhs')$
                              if (lhs/=lhs') then
                                error ("For some reason, the two LHSs of the same component do not match! -> "++show (lhs,lhs'))
                              else
                                --we can now, put the rhs in the equations!
                                let
                                  equations'= Map.insert lhs' rhs' equations
                                  --queue' = rest++(getRHSVariables equation)
                                  queue' = (getRHSVariables equation)++rest
                                in
                                  --Debug.trace("queue': "++show (queue'))$
                                  enhanceHelper queue' equations'

--type Equation = ((Either Variable Expression, Int), Either (Maybe Bool) [(Expression,Int)])
tseitinEncoding::Int ->AIG->Equation->(Maybe (Int,Int),[String])
tseitinEncoding k aig@(AIG numVariables numInputs latches outputs gates description) e =
  let
    numOutputs = length outputs
    lito = \o -> case findIndex (o==) outputs of Just i -> (1+i)
    litv = getVariableLiteral numVariables numOutputs
    lite = getExpressionLiteral numVariables numOutputs
    lita = getAttackerLiteral numVariables numOutputs
    getFlag = getAttackerFlag numVariables  numOutputs k--getAttackerFlag
  in
    case e of
      ((Right (Exp e),t), Left _ )                -> error ("Equation "++ show e++" assigns a value to an expression. why does this happen?" )
      ((Right (Exp lhs),t), Right [(Exp rhs,t')]) -> (Nothing,[(addLiteral $ lito (Output (Exp lhs)) )++", ",(addLiteral (- (lite (Exp rhs,t'))))++", " ])--(Nothing,[(addLiteral (- (lite (Exp rhs,t'))))++", "++(addLiteral (0))++";"])--this is the equation of an output (it starts with Right), we will check for sat of negation of RHS
      ((Left (Var i),t), Left Nothing )           -> (Nothing,[])-- Variable vi(t) is an input, we leave it as an unrestricted literal--[(addLiteral (-(litv (Var i,t))))++", "++(addLiteral ((litv (Var i,t))))++", "++(addLiteral (0))++";",(addLiteral ((litv (Var i,t))))++", "++(addLiteral (-(litv (Var i,t))))++", "++(addLiteral (0))++";"]

      ((Left (Var i),t), Left (Just False))       ->
        let
          lhs      = (litv (Var i,t))
        in
          if i == 0 then
            --we are talking about variable 0 (i.e, false)
            --(Nothing,[(assumeLiteral (-lhs))++";"])
                                                    (Nothing,[(addLiteral (-lhs))++", "++(addLiteral (0))++";"])

          else
        --does this equation correspond to a latch?
            case Map.lookup i description of
              Just (Latch v e _)   ->
                let
                  attacker = lita (Var i,t)
                  flag = getFlag i
                in
                  (Just (i,flag),--we should return which component is vulnerable and what flag corresponds to it.
                    [(addLiteral (flag))++", "++(addLiteral (-lhs))++", "++(addLiteral (0))++";", --Attacker is not present, require -lhs
                     --(addLiteral (-flag))++", "++(addLiteral (attacker))++", "++(addLiteral (0))++";"])
                     (addLiteral (-flag))++", "++(addLiteral (-lhs))++", "++(addLiteral (attacker))++", "++(addLiteral (0))++";", --attacker is present, make lhs = attackerInput
                     (addLiteral (-flag))++", "++(addLiteral (lhs))++", "++(addLiteral (-attacker))++", "++(addLiteral (0))++";"
                     ])
              Just _     -> --[(assumeLiteral (- (litv (Var i,t))))++";"]--assume vi(t)=False, alternatively, add negative literal
                  (Nothing,[(addLiteral (-lhs))++", "++(addLiteral (0))++";"])--assume vi(t)=False, alternatively, add negative literal
              otherwise -> error("Failed to lookup Var "++show i++" in description map "++show description)

      ((Left (Var i),t), Left (Just True))        -> --[(assumeLiteral ((litv (Var i,t))))++";"]--assume vi(t)=False, alternatively, add negative literal
        let
          lhs      = (litv (Var i,t))
        in
          if i == 0 then
            --we are talking about the negation of variable 0 (i.e, true)
            error ("You are trying to assume v0, which is supposed to be always false!")
          else
        --does this equation correspond to a latch?
            case Map.lookup i description of
              Just (Latch _ _ _)   ->
                let
                  attacker = lita (Var i,t)
                  flag = getFlag i
                in
                  (Just (i,flag),
                  [(addLiteral (flag))++", "++(addLiteral (lhs))++", "++(addLiteral (0))++";",--Attacker is not present, require lhs
                   --(addLiteral (-flag))++", "++(addLiteral (attacker))++", "++(addLiteral (0))++";"
                   (addLiteral (-flag))++", "++(addLiteral (-lhs))++", "++(addLiteral (attacker))++", "++(addLiteral (0))++";",
                   (addLiteral (-flag))++", "++(addLiteral (lhs))++", "++(addLiteral (-attacker))++", "++(addLiteral (0))++";"
                   ])
              Just _     ->
                  (Nothing,[(addLiteral (lhs))++", "++(addLiteral (0))++";"])--assume vi(t)=True.
              otherwise -> error("Failed to lookup Var "++show i++" in decription map "++show description)

      ((Left (Var i),t), Right [(Exp e,t')])      ->
        --Confirm that this is a latch that we need to equalise.
        case Map.lookup i description of
          Just (Latch _ _ _)   ->
            let
              lhs = litv (Var i,t)
              rhs = lite (Exp e,t')
              attacker = lita (Var i,t)
              flag = getFlag (i)
            in
              --the tseitin encoding for an equality is a double implication (¬lhs or rhs ) and (lhs or ¬rhs)
              (Just (i,flag),
              [
                (addLiteral (flag))++", "++(addLiteral (-lhs))++", "++(addLiteral (rhs))++", "++(addLiteral (0))++";", --if attacker not present
                (addLiteral (flag))++", "++(addLiteral (lhs))++", "++(addLiteral (-rhs))++", "++(addLiteral (0))++";",--if attacker not present
                --(addLiteral (-flag))++", "++(addLiteral (attacker))++", "++(addLiteral (0))++";" --if attacker is present
                (addLiteral (-flag))++", "++(addLiteral (-lhs))++", "++(addLiteral (attacker))++", "++(addLiteral (0))++";", --if attacker is present
                (addLiteral (-flag))++", "++(addLiteral (lhs))++", "++(addLiteral (-attacker))++", "++(addLiteral (0))++";" --if attacker is present
              ])
          otherwise -> error("I was expecting a latch for Var "++show i)

      ((Left (Var i),t), Right [(Exp e1,t1),(Exp e2,t2)])       ->
       --This is gate! no need to guard these.
         let
           lhs = litv (Var i,t)
           rhs1 = lite (Exp e1,t1)
           rhs2 = lite (Exp e2,t2)
         in
           --the tseitin encoding for an and gate is a (¬rhs1, ¬rhs2,lhs)(rhs1,¬lhs)(rhs2,¬lhs)
           (Nothing,[
           (addLiteral (lhs))++", "++(addLiteral (-rhs1))++", "++(addLiteral (-rhs2))++", "++(addLiteral (0))++";",
           (addLiteral (-lhs))++", "++(addLiteral (rhs1))++", "++(addLiteral (0))++";",
           (addLiteral (-lhs))++", "++(addLiteral (rhs2))++", "++(addLiteral (0))++";"
           ])

addLiteral::Int->String
addLiteral i = "solver->add("++show i++")"

-- assumeLiteral::Int->String
-- assumeLiteral i = "solver->assume("++show i++")"

getVariableLiteral::Int->Int->(Variable,Int)-> Int
getVariableLiteral numVariables numOutputs (Var i,t) = (1+numOutputs+(2*numVariables*t)+i) --we need to add 1 to prevent variables refer to 0

getAttackerLiteral::Int->Int->(Variable,Int)-> Int
getAttackerLiteral numVariables numOutputs (Var i,t) = (1+numOutputs+numVariables+(2*numVariables*t)+i) --we need to add 1 to prevent variables refer to 0

getAttackerFlag::Int->Int->Int->Int-> Int
getAttackerFlag numVariables numOutputs k varIndex = (1+numOutputs+(2*numVariables*(k+1))+varIndex)

getExpressionLiteral::Int->Int->(Expression,Int)-> Int
getExpressionLiteral numVariables numOutputs (Exp e,t) =
    let
      res =
        if even e then
          --expression is even, we can refer to the positive
          (1+numOutputs+(2*numVariables*(t)+(e `div` 2)))
        else
          -(1+numOutputs+(2*numVariables*(t)+(e `div` 2)))
    in
      if res ==0 then
        error ("Something is wrong with expression "++show (e,t))
      else
        res
   --we need to add 1 to prevent variables refer to 0

toEquation:: Int-> Component -> Equation
toEquation k component =
  case component of
    (Input i)          -> ((Left i,k),Left Nothing)              -- Inputs don't depend on other elements
    (Latch i rhs v0)    ->
      if k ==0 then
          case v0 of
          (Exp 0) -> ((Left i,0) , Left (Just False))
          (Exp 1) -> ((Left i,0) , Left (Just True))
          othwerwise -> error ("Initial value of Latch is not boolean!")
      else
        ((Left i,k),Right [(rhs,k-1)])         -- Latches connect time steps: lhs(k+1)= rhs(k)
    (Gate i  rhs1 rhs2) -> ((Left i,k),Right [(rhs1,k),(rhs2,k)]) -- gates always update in the same timestep
    Output o           -> ((Right o,k), Right[(o,k)])                 -- o(t)=exp(t), but exp(t)= either v_i(t) or ¬v_i(t), so it resolves.

buildCones:: AIG-> (Int -> [Int], Int -> [Int])
buildCones (AIG numVariables inputs latches outputs gates description) =
  let
    (Graph variables predecessors successors) = buildGraph numVariables latches gates
    --Tarjan's algorithm tranforms a graph into a DAG by lifting states to SCCs
    --Prime ' means dual, results from switching successors with predecessors, flipping the arrows of the graph
    (toSCC,sccGraph@(Graph sccs sccSuccessors sccPredecessors)) = tarjan (Graph variables predecessors successors)  -- ::(Map.Map Int (SCC Int), Graph (SCC Int))

    (toSCC',sccGraph'@(Graph sccs' sccSuccessors' sccPredecessors')) = tarjan (Graph variables successors predecessors)  -- ::(Map.Map Int (SCC Int), Graph (SCC Int))

    (sources, sinks, initialLabels)     = getInitialLabels ((map getVariable inputs )++(map getVariable latches)) sccGraph  toSCC
    (sources',sinks',initialLabels')    = getInitialLabels ((map getVariable outputs)++(map getVariable latches)) sccGraph' toSCC'

    toLabelledCOI  = getCOIMap sinks  sccGraph  initialLabels
    toLabelledCOI' = getCOIMap sinks' sccGraph' initialLabels'

    coi = (\v-> sort $ concat $ map (\i-> Set.elemAt i sources)  (translateCOI $ toLabelledCOI Map.! (toSCC Map.! v )))::Int->[Int]
    ioc = (\v-> sort $ concat $ map (\i-> Set.elemAt i sources') (translateCOI $ toLabelledCOI' Map.! (toSCC' Map.! v )))::Int->[Int]
  in
    (coi,ioc)


buildAIG::String -> AIG
buildAIG aiger =
  let
    (numVariables,numInputs,numLatches,numOutputs,numGates,numBadVariables,numConstraints) = parseAIGERHeader (head (lines aiger))
    --Inputs need not be processed, and can be ignored           --I
    latchesAIG      = take numLatches $ drop (numInputs) $ tail $ lines aiger            --L (tail because we drop the header)
    outputsAIG      = take numOutputs $ drop (numLatches+numInputs) $ tail $ lines aiger --O
    badVariablesAIG = take numBadVariables $ drop (numOutputs+numLatches+numInputs) $ tail $ lines aiger             --B
    constraintsAIG  = take numConstraints $ drop (numBadVariables+numOutputs+numLatches+numInputs) $ tail $ lines aiger    --C
    gatesAIG        = take numGates $ drop (numConstraints+numBadVariables+numOutputs+numLatches+numInputs) $ tail $ lines aiger     --A
    --The following lines put the info in the right data structures.
    inputs        = getInputs numInputs             -- Translate the expression to a variable index
    latches       = getLatches latchesAIG           -- A latch has lhs, rhs and initial value. The equation is of the form  l(t+1)= expr(t)
    gates         = getGates gatesAIG               -- A gate has lhs and rhs. The equation is of the form                  g(t)  = expr1(t) and expr2(t)
    badVariables  = getBadVariables badVariablesAIG -- a bad variable is just an expression. We will transform them into outputs.
    outputs       = nub$ map toOutput badVariables
    description   = Map.fromList $ map (\c -> (getVariable c,c)) (inputs++(latches++gates))
  in
    AIG numVariables inputs latches outputs gates description
      where
        toOutput (BadVariable (Exp expr)) =
          if even expr then
            --when the bad variable is true, then there is a problem. When an output is false, there is a problem.
             Output (Exp (expr+1)) -- +1 negates the variable referred by an even expression
          else
             Output (Exp (expr-1)) -- -1 negates the variable referred by an odd expression

parseAIGERHeader::String -> (Int,Int,Int,Int,Int,Int,Int)
parseAIGERHeader header =
   let tokens = words header
       len = length tokens
   in
   let
--    filetype = if tokens!!0 == "aag" then [Ascii] else if tokens!!0 == "aag" then [Binary] else []
      numVars = (read (tokens!!1))
      numInputs = (read (tokens!!2))
      numLatches = (read (tokens!!3))
      numOutputs = (read (tokens!!4))
      numGates = (read (tokens!!5))
      numSafety = if len > 6 then (read (tokens!!6)) else (0)
      numCons = if len > 7 then (read (tokens!!7)) else (0)
  in
       (numVars,numInputs,numLatches,numOutputs,numGates,numSafety,numCons)

getInputs::Int -> [Component]
getInputs numInputs =
  map (Input . Var) (take numInputs $ [1..])

getLatches::[String]-> [Component]
getLatches latches = map parseLatch latches

parseLatch::String ->  Component
parseLatch latchDesc=
  let
    tokens = words latchDesc
    i = Var $ (read (tokens!!0)) `div` 2
    e = Exp $ read (tokens!!1)
    v0 =
      if length tokens == 3 then
        if (read (tokens!!2)) == 1 then
          Exp 1
        else
          Exp 0
      else
        Exp 0
  in
    Latch i e v0

getGates::[String] -> [Component]
getGates gates = map parseGate gates

parseGate::String->  Component
parseGate gateDesc=
  let
    tokens = words gateDesc
    i = Var $ (read (tokens!!0)) `div` 2
    e1 = Exp $read (tokens!!1)
    e2 = Exp $read (tokens!!2)
  in
      Gate i e1 e2

--a bad variable is just going to be an expression.
getBadVariables::[String]->[Component]
getBadVariables badVariables = map parseBadVariable badVariables

parseBadVariable ::String ->  Component
parseBadVariable badVariableDesc=
  let tokens = words badVariableDesc
    in
      BadVariable $ Exp (read (tokens!!0))

buildGraph:: Int-> [Component]->[Component]->(Graph Int)
buildGraph numVariables latches gates =
  let
    vertices = Set.fromList [0..numVariables]
    predecessorsInit = Map.fromList (map (\i -> (i,Set.empty)) [0..numVariables])
    successorsInit = Map.fromList (map (\i -> (i,Set.empty)) [0..numVariables])
    (predecessors,successors)   = latchesAdjacency latches predecessorsInit successorsInit
    (predecessors',successors') = gatesAdjacency gates predecessors successors
  in
    (Graph vertices predecessors' successors')

latchesAdjacency::[Component]->Map.Map Int (Set.Set Int)->Map.Map Int (Set.Set Int)-> (Map.Map Int (Set.Set Int),Map.Map Int (Set.Set Int))
latchesAdjacency latches predecessors successors =
  let
    latchEdges = (map parseLatchEdge latches)
    predecessors'= foldr (\(Edge r c) -> (Map.adjust (Set.insert r) c)) predecessors latchEdges
    successors'= foldr (\(Edge r c) -> (Map.adjust (Set.insert c) r)) successors latchEdges
  in
    (predecessors',successors')

gatesAdjacency::[Component] -> Map.Map Int (Set.Set Int)->Map.Map Int (Set.Set Int)-> (Map.Map Int (Set.Set Int),Map.Map Int (Set.Set Int))
gatesAdjacency gates  predecessors successors =
  let
    gateEdges = (map parseGateEdges gates)
    predecessors'= foldr (\(Edge r c) -> (Map.adjust (Set.insert r) c)) predecessors (concat gateEdges)
    successors'= foldr (\(Edge r c) -> (Map.adjust (Set.insert c) r)) successors (concat gateEdges)
  in
    (predecessors',successors')

--Edges follow flow of information i.e., from rhs to lhs
parseLatchEdge::Component -> Edge
parseLatchEdge (Latch (Var i) (Exp e) _) = Edge (e `div` 2) i

parseGateEdges::Component -> [Edge]
parseGateEdges (Gate (Var i) (Exp e1) (Exp e2) ) =
  [Edge (e1 `div` 2) i, Edge (e2 `div` 2) i]

tarjan::(Show a,Ord a)=> Graph a -> (Map.Map a (SCC a), Graph (SCC a))
tarjan (Graph vertices predecessors successors) =
  let
    verticesList = Set.toList vertices
    initState = TarjanState Map.empty Map.empty Set.empty 0 [] [] Map.empty
    finalState = foldr (\v ts-> strongConnect successors v ts) initState verticesList
    sccSuccessors = Map.fromList [(scc,successor)| scc<-(sccs finalState),successor <- [findSuccessorSCCs scc successors (sccMap finalState)]]
    sccPredecessors = Map.fromList [(scc,predecessor)| scc<-(sccs finalState),predecessor <- [findPredecessorSCCs scc predecessors (sccMap finalState)]]
  in
    (sccMap finalState, Graph (Set.fromList $ sccs finalState) sccSuccessors sccPredecessors)

findSuccessorSCCs::(Show a,Ord a)=>(SCC a)->Map.Map a (Set.Set a)-> Map.Map a (SCC a) -> Set.Set (SCC a)
findSuccessorSCCs scc successors sccMap =
  let
    --for each variable in scc, get its successor set
    listOfSuccessorsSets = map (\v-> successors Map.! v) scc
    --we need to join the successors to remove any duplicates
    listOfSuccessors = foldr (\set acc-> Set.union acc set) (Set.empty) listOfSuccessorsSets
    successorSCCs = Set.map (\v-> sccMap Map.! v) listOfSuccessors
  in
    successorSCCs

findPredecessorSCCs::(Show a,Ord a)=>(SCC a)->Map.Map a (Set.Set a)-> Map.Map a (SCC a) -> Set.Set (SCC a)
findPredecessorSCCs scc predecessors sccMap =
  let
    --for each variable in scc, get its predecessor set
    listOfPredecessorsSets = map (\v-> predecessors Map.! v) scc
    --we need to join the predecessors to remove any duplicates
    listOfPredecessors = foldr (\set acc-> Set.union acc set) (Set.empty) listOfPredecessorsSets
    predecessorSCCs = Set.map (\v-> sccMap Map.! v) listOfPredecessors
  in
    predecessorSCCs

strongConnect::(Show a,Ord a)=>(Map.Map a (Set.Set a)) -> a-> TarjanState a -> TarjanState a--(AdjacencySet (Set.Set Int))
strongConnect successors v state@(TarjanState indexMap lowlinkMap onStack index stack sccs sccMap) =
  case Map.lookup v indexMap of
    (Just i) -> state
    Nothing ->
  --this is function is a composition (scc . loop . init) params. init returns state', loop returns state'', and we return state'''
      let
        indexMap' = Map.insert v index indexMap
        lowlinkMap' = Map.insert v index lowlinkMap
        index' = index+1
        stack' = v:stack
        onStack' = Set.insert v onStack
        state' = TarjanState indexMap' lowlinkMap' onStack' index' stack' sccs sccMap
        in
          case Map.lookup v successors of
            --Nothing -> error("A vertex "++(show v)++"has Nothing successors! It should instead have Just []")
            (Just (successors_v)) ->
              let
                state''@(TarjanState indexMap'' lowlinkMap'' onStack'' index'' stack'' sccs'' sccMap'') = strongConnectLoop successors v (Set.toList successors_v) state'
                Just lowlink_v'' = Map.lookup v lowlinkMap''
                Just index_v'' = Map.lookup v indexMap''
              in
                if lowlink_v'' == index_v'' then
                  sccLoop v state''
                else
                  state''

translateCOI::[Bool]->[Int]
translateCOI coi = translateCOIHelper coi 0
  where translateCOIHelper [] k = []::[Int]
        translateCOIHelper (v:vs) k =  if v then k:(translateCOIHelper vs (k+1)) else translateCOIHelper vs (k+1)


indexedCOI::[Bool]->[(Int,Bool)]
indexedCOI coi = indexedCOIHelper coi 0
  where indexedCOIHelper [] k = []::[(Int,Bool)]
        indexedCOIHelper (v:vs) k =  (k,v):(indexedCOIHelper vs (k+1))

getNiceCOI::Maybe [Bool]->[Int]
getNiceCOI Nothing = []
getNiceCOI (Just coi) = translateCOI coi

getLessNiceCOI::Maybe [Bool]->[(Int,Bool)]
getLessNiceCOI Nothing = []
getLessNiceCOI (Just coi) = niceCOIHelper coi 0
  where niceCOIHelper [] k = []::[(Int,Bool)]
        niceCOIHelper (v:vs) k =  (k,v):(niceCOIHelper vs (k+1))

sccLoop::(Show a,Ord a)=>a -> TarjanState a -> TarjanState a
sccLoop v state@(TarjanState indexMap lowlinkMap onStack index stack sccs sccMap) =
  let
    (prefix, suffix) = break (== v) stack -- v is on the second part of the stack
    scc = (v:prefix) --TODO: maybe you want to invert this?
    stack'=tail suffix
    --scc = foldr (\w -> Set.insert w) (Set.empty) (v:prefix) --no need to do folding anymore, since the previous fold was just to put all elements in a set.
    onStack' = foldr (\w -> Set.delete w) onStack scc --We remove all elements in the scc from the stack
    sccs' = (scc:sccs)
    sccMap' = foldr (\w-> Map.insert w scc) sccMap scc --We tell all elements in the scc that their SCC is scc via the sccMap
  in
    (TarjanState indexMap lowlinkMap onStack' index stack' sccs' sccMap')

strongConnectLoop::(Show a,Ord a)=>Map.Map a (Set.Set a) -> a-> [a]->TarjanState a ->TarjanState a
strongConnectLoop successors v [] state = state
strongConnectLoop successors v (w:ws) state@(TarjanState indexMap lowlinkMap onStack index stack sccs sccMap) =
  if Map.lookup w indexMap == Nothing then
    let
      state'@(TarjanState indexMap' lowlinkMap' onStack' index' stack' sccs' sccMap') = strongConnect successors w state
      Just lowlink_v' = Map.lookup v lowlinkMap'
      Just lowlink_w' = Map.lookup w lowlinkMap'
      lowlinkMap'' = Map.insert v (min lowlink_v' lowlink_w') lowlinkMap'
    in
      strongConnectLoop successors v ws (TarjanState indexMap' lowlinkMap'' onStack' index' stack' sccs' sccMap')
  else
    if (Set.member w onStack) then
      let
        Just lowlink_v = Map.lookup v lowlinkMap
        Just index_w = Map.lookup w indexMap
        lowlinkMap' = Map.insert v (min lowlink_v index_w) lowlinkMap
      in
        strongConnectLoop successors v ws (TarjanState indexMap lowlinkMap' onStack index stack sccs sccMap)
    else
        strongConnectLoop successors v ws state

getInitialLabels::[Int]->Graph (SCC Int)->Map.Map Int (SCC Int)->(Set.Set (SCC Int),Set.Set (SCC Int),Map.Map (SCC Int) [Bool])
getInitialLabels variables (Graph sccs sccSuccessors sccPredecessors) sccMap =
  let
    variableSccs = map (\i -> sccMap Map.! i) variables
    allSources = Set.fromList (variableSccs)
    allSinks = Set.filter (\scc -> ((sccSuccessors Map.! scc) == Set.empty) || ((sccSuccessors Map.! scc) == Set.singleton (scc))) sccs
    allZeroes = take ((Set.size allSources)) (repeat False)
    initialMap = Map.fromList [(v,label)|v <- (Set.toList sccs) ,label <- [allZeroes]]
    --Now, for every source variable, we find its SCC and color its label accordingly
    mapWithSources = Set.foldr (\source m-> Map.adjust (zipWith (||) (labelOfSource source allSources)) (source) m) initialMap (allSources)
  in
    (allSources,allSinks,mapWithSources)

getCOIMap::Set.Set (SCC Int)-> Graph (SCC Int) -> Map.Map (SCC Int) [Bool] -> Map.Map (SCC Int) [Bool]
getCOIMap sccSinks sccGraph@(Graph sccs sccSuccessors sccPredecessors) initialSourceLabels =
  let
      (visited',labelsMap') = Set.foldr (\p (v,l) -> getCOI p sccGraph (v,l)) (Set.empty,initialSourceLabels) sccSinks
  in
      labelsMap'

getCOI::(SCC Int)-> Graph (SCC Int) -> (Set.Set (SCC Int),Map.Map (SCC Int) [Bool])->(Set.Set (SCC Int),Map.Map (SCC Int) [Bool])
getCOI scc sccGraph@(Graph sccs sccSuccessors sccPredecessors) (visited,labels) =
  let
      (visited',labelsMap') = getCOILoop sccGraph [scc] (visited,labels)
  in
      (visited',labelsMap')

getCOILoop::Graph (SCC Int) -> [SCC Int]-> (Set.Set (SCC Int),Map.Map (SCC Int) [Bool]) -> (Set.Set (SCC Int),Map.Map (SCC Int) [Bool])
getCOILoop sccGraph [] (visited,labelsMap) =  (visited,labelsMap)
getCOILoop sccGraph@(Graph sccs sccSuccessors sccPredecessors) queue@(c:cs) (visited,labelsMap)  =
  --Debug.trace("Queue size "++(show (length queue)))$
  if Set.member c visited then
    getCOILoop sccGraph cs (visited,labelsMap) -- you already visited c, just continue
  else
    let
      --You have not visited this state!
      visited' = Set.insert c visited  -- mark the current node as visited.
      (visited'',labelsMap')= Set.foldr (\p (v,l) -> getCOI p sccGraph (v,l)) (visited',labelsMap) (sccPredecessors Map.! c)
      label = (labelsMap Map.! c)
      label' = foldr (mergeLabels labelsMap') label (sccPredecessors Map.! c)
      labelsMap'' = Map.insert c label' labelsMap'
    in
      getCOILoop sccGraph cs (visited'',labelsMap'')

mergeLabels::Map.Map (SCC Int) [Bool] -> SCC Int-> [Bool] ->[Bool]
mergeLabels labelsMap scc lbl =
  --case Map.lookup scc labelsMap of
  case Map.lookup scc labelsMap of
    Nothing -> lbl
    (Just scc_lbl) -> zipWith (||) scc_lbl lbl

labelOfSource::(SCC Int)->Set.Set (SCC Int)->[Bool]
labelOfSource src sources =
  --Debug.trace("checking label of "++show (v))$
  case Set.lookupIndex src sources of
    (Just index)->((take (index)) (repeat False)) ++ [True] ++ ((take ((Set.size sources)- index+1)) (repeat False))
    Nothing -> error ("This should not happen!")
