#include "../../cadical/src/cadical.hpp"
#ifdef NDEBUG
#undef NDEBUG
#endif
#include <cassert>
#include <map>
#include <set>
#include <queue>
#include <chrono>
#include <string>
#include <array>
#include <iterator>
#include <iostream>
using namespace std;

TEMPLATE_HEADER

void showAttacker(array<bool,numComponents> attacker){
    for(auto it: attacker){cout<<it;}
    cout<<endl;
}

set<array<bool,numComponents>> successors(array<bool,numComponents> s)
{
    set<array<bool,numComponents>> succs;
    bool elem;
    int index=0;
    int count=0;
    for (auto it=s.begin(); it != s.end(); ++it)
    {
        elem = *it;
        if (elem)
        {
            count++;
        }
    }
    elem = false;
    for (auto it=s.begin(); it != s.end() && !elem ; ++it,index++)
    {
        elem = *it;
        if (!elem)
        {
            array<bool,numComponents> c_s = s;
            c_s[index]=true;
            succs.insert(c_s);
        }
    }
    if(count<maxFailComponents)
        return succs;
    set<array<bool,numComponents>> no_succs;
    return no_succs;
}

bool contained(array<bool,numComponents> s, array<bool,numComponents> r)
{
    for (int i=0; i<numComponents; i++)
    {
        if(!((!s[i])||r[i]))
            return false;
    }
    return true;
}

void enableRequirement(int requirement,CaDiCaL::Solver * solver){
    for(int i =0; i<numRequirements; i++)
    {
        if(i!=requirement)
            solver->assume(requirementToLiteral[requirements[i]]);
        else
            solver->assume(-(requirementToLiteral[requirements[i]]));
    }
    return;
}

int testAttacker(int requirement, array<bool,numComponents> attacker,CaDiCaL::Solver * solver){
    //This method creates a set of assumptions for the solver, then asks the solver to solve the problem
    enableRequirement(requirement,solver);
    for(int i =0; i<numComponents; i++)
    {
        if(attacker[i])
            solver->assume(attackers[i]);
        else
            solver->assume(-(attackers[i]));
    }
    return (solver->solve ());
}

void epoch(int requirement,queue<array<bool,numComponents>> * agenda,CaDiCaL::Solver * solver, std::chrono::time_point<std::chrono::steady_clock>  start)
{
    queue<array<bool,numComponents>> agenda_;
    double delta;
    auto end = chrono::steady_clock::now();
    while (!(agenda->empty()))
    {
        end = chrono::steady_clock::now();
        delta = chrono::duration_cast<chrono::milliseconds>(end - start).count();
        if(delta>timeout)
        {
            //timeout for the requirement occurred.
            queue<array<bool,numComponents>> agenda_1;
            (*agenda)=agenda_1;
            return;
        }
        array<bool,numComponents> attacker = agenda->front();
        agenda->pop();
        //We need to see if there is a filter that fits it because we added elements that may have completed a minimal set
        // HERE IS WHERE MONOTONICITY PLAYS A ROLE. it is disabled for the tests without monotonicity
        //    bool isContained = false;
        //    for(auto attackers_it = successfulAttackers->begin(); attackers_it!= successfulAttackers->end() && !isContained; ++attackers_it)
        //    {
        //        array<bool,numComponents> successfulAttacker = *attackers_it;
        //        if(contained(successfulAttacker,attacker))
        //            isContained=true;
        //    }
        //    if(isContained)
        //        continue;
        //attacker does not have a minimal attacker. We check it.
        if(testAttacker(requirement,attacker,solver)==10)
        {
            //successfulAttackers->insert(attacker); //no need to add successors to queue.
            //write to log
            cout<<"S ";
            if(numComponents>0)
            {
                for(int i =0; i<numComponents; i++)
                {
                    if(attacker[i])
                        cout<<components[i]<<" ";
                }
            }
            cout<<"0"<<endl;
           //We also comment this line when we disable monotonicity, because we put successors in the agenda as well.
            //continue;
        }
        else
        {
            cout<<"F ";
            if(numComponents>0)
            {
                for(int i =0; i<numComponents; i++)
                {
                    if(attacker[i])
                        cout<<components[i]<<" ";
                }
            }
            cout<<"0"<<endl;
        }
        end = chrono::steady_clock::now();
        delta = chrono::duration_cast<chrono::milliseconds>(end - start).count();
        if(delta>timeout)
        {
            //timeout for the requirement occurred.
            queue<array<bool,numComponents>> agenda_1;
            (*agenda)=agenda_1;
            return;
        }
        //this attacker is not minimal. We need to extend it.
        set<array<bool,numComponents>>attacker_successors = successors(attacker);
        for(auto succ_it= attacker_successors.begin(); succ_it != attacker_successors.end(); ++succ_it)
        {
            //Do not insert in current agenda. Wait until we process all minimal candidates.
            agenda_.push(*succ_it);
        }
    }
    //change reference to the new agenda.
    (*agenda)=agenda_;
}

int main () {
    auto start = chrono::steady_clock::now();
    cout<<"!"<<numRequirements<<endl;
    CaDiCaL::Solver * solver = new CaDiCaL::Solver;
    
    TEMPLATE_SYSTEM_DESCRIPTION
    
    auto end = chrono::steady_clock::now();
    array<bool,numComponents> zeroAttacker;
    for(int i=0; i<numComponents; i++)
        zeroAttacker[i]=false; //Inits are necessary.
    array<bool,numComponents> oneAttacker;
    for(int i=0; i<numComponents; i++)
        oneAttacker[i]=true; //Inits are necessary.
    queue<array<bool,numComponents>> agenda; //This can be improved if we use a queue
    double delta;
    //We need to iterate over every requirement
    for(int requirement = 0; requirement< numRequirements; requirement++)
    {
        cout<<"#"<<requirements[requirement]<<endl;
        start = chrono::steady_clock::now();
        //first, we check if the requirement is vulnerable at all!
        enableRequirement(requirement,solver);
        
        if(testAttacker(requirement,oneAttacker,solver)==20)
        {
            //this requirement cannot be broken by any attacker model
            //Write to log
            cout<<"F ";
            for(int i =0; i<numComponents; i++)
            {
                if(oneAttacker[i])
                    cout<<components[i]<<" ";
            }
            cout<<"0"<<endl;
            end = chrono::steady_clock::now();
            cout<<"@"<< chrono::duration_cast<chrono::milliseconds>(end - start).count()<< endl;
            //<< " ms" << endl;
            //cout<<"@"<< chrono::duration_cast<chrono::seconds>(end - start).count()<< endl;
            continue;
        }
        //otherwise, we look for candidate attackers
        //Write to log
        cout<<"S ";
        for(int i =0; i<numComponents; i++)
        {
            if(oneAttacker[i])
                cout<<components[i]<<" ";
        }
        cout<<"0"<<endl;
        end = chrono::steady_clock::now();
        delta = chrono::duration_cast<chrono::milliseconds>(end - start).count();
        if(delta>timeout)
        {
            //timeout for the requirement occurred.
            delete solver;
            return 0;
        }
        agenda.push(zeroAttacker);
        while(!agenda.empty())
        {
            epoch(requirement,&agenda,solver, start);
        }
        end = chrono::steady_clock::now();
        cout<<"@"<< chrono::duration_cast<chrono::milliseconds>(end - start).count()<< endl;
//        << " ms" << endl;
        //cout<<"@"<< chrono::duration_cast<chrono::seconds>(end - start).count()<< endl;
        //success.clear();
    }
    delete solver;
    return 0;
}
