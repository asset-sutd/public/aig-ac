
module Main
(
  main
)
where
import System.Environment
import System.Exit
import Data.List
import AIGtoCPP

main = do
  args <- getArgs
  if args==[] then
    printUsage
  else
    case find (\x -> x=="-h" || x=="--help") args of
      Just _ -> printUsage
      Nothing ->
        do
          let
            timeSteps =
              case findIndex (=="-k") args of
                Nothing -> 10
                Just x -> read (args!!(x+1))::Int
            components =
              case findIndex (=="-c") args of
                Nothing -> 3
                Just x -> read (args!!(x+1))::Int
            timeout =
              case findIndex (=="-t") args of
                Nothing -> 600000
                Just x -> read (args!!(x+1))::Int
            -- attackers =
            --   case findIndex (=="-a") args of
            --     Nothing -> 20
            --     Just x -> read (args!!(x+1))::Int
            isolation =
              case findIndex (=="-i") args of
                Nothing -> True
                Just _ -> False
            monotonicity =
              case findIndex (=="-m") args of
                Nothing -> True
                Just _ -> False
            clustering =
              case findIndex (=="-u") args of
                Nothing -> True
                Just _ -> False
            benchmark = last args
            params = Params benchmark timeSteps components clustering isolation monotonicity timeout
          process params

printUsage = putStrLn "Usage: [-k Int] [-c Int] [-t Int] [-i] [-m] [-u] file"
