#!/bin/sh

#--------------------------------------------------------------------------#

die () {
  echo "${HIDE}run.sh:${NORMAL} ${BAD}error:${NORMAL} $*"
  exit 1
}

msg () {
  echo "${HIDE}run.sh:${NORMAL} $*"
}

for dir in . .. ../..
do
  [ -f $dir/scripts/colors.sh ] || continue
  . $dir/scripts/colors.sh || exit 1
  break
done

#--------------------------------------------------------------------------#

[ -d ../../cadical/build ] || \
die "Could not locate CaDiCaL build. From this directory, CaDiCaL build should be on the relative path \"../../cadical/build\""

[ x"$CADICALBUILD" = x ] && CADICALBUILD="../../cadical/build"

[ -f "$CADICALBUILD/makefile" ] || \
  die "can not find '$CADICALBUILD/makefile' (run 'configure' first)"

[ -f "$CADICALBUILD/libcadical.a" ] || \
  die "can not find '$CADICALBUILD/libcadical.a' (run 'make' first)"

echo -n "$HILITE"
echo "---------------------------------------------------------"
echo "SAT Solving"
echo "---------------------------------------------------------"
echo -n "$NORMAL"

#make -C $CADICALBUILD
#res=$?
#[ $res = 0 ] || exit $res

#--------------------------------------------------------------------------#

makefile=$CADICALBUILD/makefile


tests=.

export CADICALBUILD

#--------------------------------------------------------------------------#

ok=0
failed=0

cmd () {
  echo $*
  $* >> $name.txt
  status=$?
  test $status = 1 && return
}

run(){
  #name=$CADICALBUILD/$1
  name=./$1
  rm -f $name.txt
  cmd $name
  result=$status
  if test $result = 0
  then
    echo "# 0 ... ${GOOD}ok${NORMAL} (normal)"
    ok=`expr $ok + 1`
  else
      if test $result = -1
      then
        echo "# 0 ... ${BAD}timeout${NORMAL} (error)"
        exit 0
      else
        echo "# 0 ... ${BAD}failed${NORMAL} (error)"
        failed=`expr $failed + 1`
      fi
  fi
  
}

#--------------------------------------------------------------------------#

RUNS_TEMPLATE

#--------------------------------------------------------------------------#

[ $ok -gt 0 ] && OK="$GOOD"
[ $failed -gt 0 ] && FAILED="$BAD"

msg "${HILITE}AIG Classification results:${NORMAL} ${OK}$ok clusters checked succesfully ${NORMAL}, ${FAILED}$failed errors.${NORMAL}"

exit $failed
