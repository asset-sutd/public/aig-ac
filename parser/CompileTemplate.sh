#!/bin/sh

#--------------------------------------------------------------------------#

die () {
  echo "${HIDE}compile.sh:${NORMAL} ${BAD}error:${NORMAL} $*"
  exit 1
}

msg () {
  echo "${HIDE}compile.sh:${NORMAL} $*"
}

for dir in . .. ../..
do
  [ -f $dir/scripts/colors.sh ] || continue
  . $dir/scripts/colors.sh || exit 1
  break
done

#--------------------------------------------------------------------------#

[ -d ../../cadical/build ] || \
die "Could not locate CaDiCaL build. From this directory, CaDiCaL build should be on the relative path \"../../cadical/build\""

[ x"$CADICALBUILD" = x ] && CADICALBUILD="../../cadical/build"

[ -f "$CADICALBUILD/makefile" ] || \
  die "can not find '$CADICALBUILD/makefile' (run 'configure' first)"

[ -f "$CADICALBUILD/libcadical.a" ] || \
  die "can not find '$CADICALBUILD/libcadical.a' (run 'make' first)"

echo -n "$HILITE"
echo "---------------------------------------------------------"
echo "Compilation"
echo "---------------------------------------------------------"
echo -n "$NORMAL"

make -C $CADICALBUILD
res=$?
[ $res = 0 ] || exit $res

#--------------------------------------------------------------------------#

makefile=$CADICALBUILD/makefile

CXX=`grep '^CXX=' "$makefile"|sed -e 's,CXX=,,'`
CXXFLAGS=`grep '^CXXFLAGS=' "$makefile"|sed -e 's,CXXFLAGS=,,'`

msg "using CXX=$CXX"
msg "using CXXFLAGS=$CXXFLAGS"

tests=.

export CADICALBUILD

#--------------------------------------------------------------------------#

ok=0
failed=0

cmd () {
  test $status = 1 && return
  echo $*
  $*
  status=$?
}

compile () {
  msg "Compiling ${HILITE}'$1'${NORMAL}"
  if [ -f $tests/$1.c ]
  then
    src=$tests/$1.c
    language=" -x c"
    COMPILE="$CXX `echo $CXXFLAGS|sed -e 's,-std=c++0x,-std=c99,'`"
  elif [ -f $tests/$1.cpp ]
  then
    src=$tests/$1.cpp
    language=""
    COMPILE="$CXX $CXXFLAGS"
  else
    die "can not find '$tests.c' nor '$tests.cpp'"
  fi
  #name=$CADICALBUILD/$1
  name=$1
  rm -f $name.o $name
  status=0
  cmd $COMPILE$language -o $name.o -c $src
  cmd $COMPILE -o $name $name.o -L$CADICALBUILD -lcadical
  result=$status
  if test $result = 0
  then
    echo "# 0 ... ${GOOD}ok${NORMAL} (ok)"
    ok=`expr $ok + 1`
  else
    echo "# 0 ... ${BAD}failed${NORMAL} (error)"
    failed=`expr $failed + 1`
  fi
}

#--------------------------------------------------------------------------#

COMPILES_TEMPLATE

#--------------------------------------------------------------------------#

[ $ok -gt 0 ] && OK="$GOOD"
[ $failed -gt 0 ] && FAILED="$BAD"

msg "${HILITE}Compilation results:${NORMAL} ${OK}$ok normal ${NORMAL}, ${FAILED}$failed errors.${NORMAL}"

exit $failed
