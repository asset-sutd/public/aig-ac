#!/bin/sh

#  
#
#  Created by Eric Rothstein on 10/10/19.
#
cd replication
cp pdtvsarmultip.aig pdtvsarmultip0.aig
cp pdtvsarmultip.aig pdtvsarmultip1.aig
cp pdtvsarmultip.aig pdtvsarmultip5.aig
cp pdtvsarmultip.aig pdtvsarmultip10.aig
cp pdtvsarmultip.aig pdtvsarmultip20.aig
cp pdtvsarmultip.aig pdtvsarmultip30.aig
cd ..

./classify.sh -k 0 replication/pdtvsarmultip0.aig
./classify.sh -k 1 replication/pdtvsarmultip1.aig
./classify.sh -k 5 replication/pdtvsarmultip5.aig
./classify.sh -k 10 replication/pdtvsarmultip10.aig
./classify.sh -k 20 replication/pdtvsarmultip20.aig
./classify.sh -k 30 replication/pdtvsarmultip30.aig

echo "This script just generated 4 files named pdtvsarmultipN-Result.txt where N is a number in 0, 1, 5, 10, 20, and 30. "
echo ""
echo "The last line of each pdtvsarmultipN-Result.txt file should contain the row of Table 1 for N steps."
echo""
echo "To obtain Tables 2 and 3, check the rows for values 4649 and 4735 in the generated result files. 4649 is the aiger expression for ¬v_2324, however, since pdtvsarmultip has 17 inputs and 130 latches, with 2324-17-130= 2177, v_2324 refers to gate g_2177. Similarly for expression 4735."
