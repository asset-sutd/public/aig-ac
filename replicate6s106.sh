#!/bin/sh

#  
#
#  Created by Eric Rothstein on 10/10/19.
#
cd replication
cp 6s106.aig 6s106-I.aig
cp 6s106.aig 6s106-M.aig
cp 6s106.aig 6s106-IM.aig
cd ..

./classify.sh replication/6s106.aig
./classify.sh -i replication/6s106-I.aig
./classify.sh -m replication/6s106-M.aig
./classify.sh -i -m replication/6s106-IM.aig

echo "This script just generated 4 txt files with prefix 6s106 and suffix Result.txt. -I means isolation was disabled, -M means monotonicity was disabled and -IM means that both were disabled."
echo ""
echo "The last line of those files shows both the average coverage and the execution time (among other data). The other lines describe information for each individual requirement."

echo "Bear in mind that the average results may vary if the VirtualMachine runs out of memory during SAT solving. In such a case, we recommend that you increase the size of the swap file and/or increase available memory."
